var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var minifyCSS = require('gulp-clean-css');
var util = require('gulp-util');
var gulpif = require('gulp-if');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var del = require('del');
var Q = require('q');
var babel = require('gulp-babel');
var runSequence = require('run-sequence');

var Pipeline = function () {
    this.entries = [];
};

Pipeline.prototype.add = function () {
    this.entries.push(arguments);
};

Pipeline.prototype.run = function (callable) {
    var deferred = Q.defer();
    var i = 0;
    var entries = this.entries;
    var runNextEntry = function () {
        // see if we're all done looping
        if (typeof entries[i] === 'undefined') {
            deferred.resolve();
            return;
        }
        // pass app as this, though we should avoid using "this"
        // in those functions anyways
        callable.apply(app, entries[i]).on('end', function () {
            i++;
            runNextEntry();
        });
    };
    runNextEntry();
    return deferred.promise;
};

var config = {
    assetsDir: 'app/Presenters/assets',
    sassPattern: 'sass/**/*.scss',
    production: !!util.env.production,
    sourceMaps: !util.env.production,
    nodeDir: 'node_modules',
    bowerDir: 'bower_components',
    revManifestPath: 'app/Presenters/assets/rev-manifest.json'
};

var app = {};

app.addStyle = function (paths, outputFilename) {
    return gulp.src(paths)
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass())
        .pipe(concat('css/' + outputFilename))
        .pipe(gulpif(config.production, minifyCSS()))
        .pipe(rev())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('www'))
        .pipe(rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};

app.addScript = function (paths, outputFilename) {
    return gulp.src(paths)
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        // .pipe(babel())
        .pipe(concat('js/' + outputFilename))
        .pipe(gulpif(config.production, uglify()))
        .pipe(rev())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('www'))
        .pipe(rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};

app.addBabelScript = function (paths, outputFilename) {
    return gulp.src(paths)
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(babel())
        .pipe(concat('js/' + outputFilename))
        .pipe(gulpif(config.production, uglify()))
        .pipe(rev())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('www'))
        .pipe(rev.manifest(config.revManifestPath, {
            merge: true
        }))
        .pipe(gulp.dest('.'));
};

app.copy = function (srcFiles, outputDir) {
    gulp.src(srcFiles)
        .pipe(gulp.dest(outputDir))
};

gulp.task('styles', function () {
    var pipeline = new Pipeline();

    pipeline.add([
        config.nodeDir + '/select2/dist/css/select2.css',
        config.nodeDir + '/select2-bootstrap-theme/dist/select2-bootstrap.css'
    ], 'select2.css');

    pipeline.add([
        config.assetsDir + '/sass/login.scss',
    ], 'login.css');

    pipeline.add([
        config.assetsDir + '/sass/contact-form.css'
    ], 'contact-form.css');

    pipeline.add([
        config.nodeDir + '/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css'
    ], 'datepicker.css');

    pipeline.add([
        config.nodeDir + '/bootstrap/dist/css/bootstrap.css',
        config.nodeDir + '/primer-navigation/build/build.css',
        config.assetsDir + '/sass/primer.scss',
        config.assetsDir + '/sass/main.scss',
        config.nodeDir + '/font-awesome/scss/font-awesome.scss'
    ], 'main.css');

    pipeline.add([
        // Datagrid css
        config.bowerDir + '/ublaboo-datagrid/assets/dist/datagrid.css',
        // Ajax spinner.
        config.bowerDir + '/ublaboo-datagrid/assets/dist/datagrid-spinners.css',
        // filter multiselect
        config.bowerDir + '/bootstrap-select/dist/css/bootstrap-select.css',
    ], 'datagrid.css');

    return pipeline.run(app.addStyle);
});

gulp.task('scripts', function () {
    var pipeline = new Pipeline();

    pipeline.add([
        config.nodeDir + '/jquery/dist/jquery.js',
        config.nodeDir + '/bootstrap/dist/js/bootstrap.js',
        config.assetsDir + '/js/nette.ajax.js',
        config.assetsDir + '/js/netteForms.js'
    ], 'main.js');

    pipeline.add([
        config.nodeDir + '/jquery/dist/jquery.js',
    ], 'jquery.js');

    pipeline.add([
        config.nodeDir + '/select2/dist/js/select2.full.js'
    ], 'select2.js');

    pipeline.add([
        config.nodeDir + '/bootstrap-datepicker/dist/js/bootstrap-datepicker.js'
    ], 'datepicker.js');

    pipeline.add([
        config.bowerDir + '/jquery/dist/jquery.js',
        config.bowerDir + '/nette-forms/src/assets/netteForms.js',
        config.bowerDir + '/nette.ajax.js/nette.ajax.js',
        config.bowerDir + '/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
        // Datagrid
        config.bowerDir + '/ublaboo-datagrid/assets/dist/datagrid.js',
        // Refreshes url on non ajax request
        config.bowerDir + '/ublaboo-datagrid/assets/dist/datagrid-instant-url-refresh.js',
        // Ajax spinner.
        config.bowerDir + '/ublaboo-datagrid/assets/dist/datagrid-spinners.js',

        config.bowerDir + '/bootstrap/dist/js/bootstrap.js',
        // filter multiselect
        config.bowerDir + '/bootstrap-select/dist/js/bootstrap-select.js',
    ], 'datagrid.js');

    return pipeline.run(app.addScript);
});

gulp.task('babelScripts', function () {
    var pipeline = new Pipeline();

    pipeline.add([
        config.assetsDir + '/js/contact.js',
    ], 'contact.js');

    pipeline.add([
        config.assetsDir + '/js/google-map-autocomplete.js',
        config.assetsDir + '/js/contact-form.js'
    ], 'contact-form.js');

    pipeline.add([
        config.assetsDir + '/js/ajaxInit.js',
    ], 'ajaxInit.js');

    return pipeline.run(app.addBabelScript);
});


gulp.task('fonts', function () {
    app.copy(
        config.nodeDir + '/font-awesome/fonts/*',
        'www/fonts'
    )
});

gulp.task('clean', function () {
    del.sync(config.revManifestPath);
    del.sync('www/css/*');
    del.sync('www/js/*');
    del.sync('www/fonts/*')
});

gulp.task('watch', function () {
    gulp.watch(config.assetsDir + '/' + config.sassPattern, ['styles']);
    gulp.watch(config.assetsDir + '/js/**/*.js', ['scripts']);
    gulp.watch(config.assetsDir + '/js/**/*.js', ['babelScripts']);
});

gulp.task('default', function (done) {
    runSequence(
        ['clean'], ['styles'], ['scripts'], ['babelScripts'], ['fonts'], ['watch'], done
    );
});
