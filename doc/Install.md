* `yarn`
* `bower install` pro data grid
* `composer install`
* `gulp --production`
* `php www/index.php orm:schema-tool:create` pro vytvoreni db schematu.
* `php www/index.php orm:fixtures:load --append` pro naimportovani 
inicializacnich dat.
* `php www/index.php app:user:create username password` pro pridani 
uzivatele admina.

