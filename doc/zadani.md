Webova aplikace pro spravu kontaktu.
====================================

# Zadani
* Vytvorit aplikaci pro spravu kontaktu.
* Vytvorit modul pro ulozeni historie zmen.
* Omezit pristup k systemu.

# Funkcni pozadavky GUI
* F1: Zobrazit veskere kontakty v systemu.
* F2: Filtrovat kontakty podle kriterii(Napr. pomoci tagu).
* F3: Vyhledat kontakt.
* F4: Pridat novy kontakt.
* F5: Odebrat stary kontakt.
* F6: Editovat kontakt.
    F6.1: Pridat/odebrat adresu.
    F6.2: Pridat/odebrat tag
    F6.3: Pridat/odebrat cislo, email, skype apod.
    F6.4: Editovat data kontaktu.
* F7: Zobrazit veskere tagy v systemu.

Kazdy kontakt je rozdelen podle statu, kde se nachazi.
* F8:  Zobrazit seznam vsech statu.
* F9:  Pridat novy stat.
* F10:  Odebrat stary stat.
* F11: Editovat stat.

* F12: Autentikace a omezeni obsahu nepovolanym uzivatelum:
    F12.1: Vytvoreni noveho uzivatele.
    F12.2: Prihlaseni uzivatele do systemu.
    F12.3: Moznost editace uzivatelu.
    F12.4: Omezeni pristupu na zaklade roli.

* F13: Logovani vsech zmen:
    F13.1: Zobrazeni vsech zmen v systemu.
    F13.2: Moznost zobrazit konkretni zmenu.

# Nefunkcni pozadavky
* N1: Dostupnost aplikace ze vsech modernich prohlizecu.
* N2: Spolehlivost aplikace.
* N3: Podpora pro vice jazykovych verzi.
* N4: PHP minimalne verze 7.0
* N5: Pouziti doctrine ORM.
