Webova sluzba pro spravu prekladu
=================================

# Zadani
* Vytvoreni webove aplikace pro spravu prekladu. (GUI)
* Vytvoreni webove sluzby, ktera tyto preklady umozni sdilet s dalsimi aplikacemi. (rest API)

# Funkcni pozadavky GUI
* F1: Zobrazit vsechny klice a jednotlive preklady.
* F2: Prelozit klice do pozadovanych jazyku.
* F3: Pridat novy klic. 
* F4: Odebrat stary klic.
* F5: Pridat novy jazyk pro preklad. 
* F6: Odebrat stary jazyk pro preklad. 
* F7: Pridat novou domenu. 
* F8: Odebrat starou domenu. 
* F9: Pridat novou skupinu/aplikaci.(kazda aplikace ma oddelene preklady)
* F10: Odebrat starou skupinu/aplikaci.
* F11: Exportovat veskere preklady pomoci rest api.
* F12: Statistika prekladu (pocet prelozenych klicu, chybejici preklady, apod.)
* FX1: Moznost importu prekladu ve formatu xliff.
* FX2: Autentikace a omezeni obsahu nepovolanym uzivatelum.
    FX2.1: Registrace noveho uzivatele.
    FX2.2: Prihlaseni uzivatele do systemu.
    FX2.3: Editace vlastniho uctu (alespon heslo).
    FX2.4: Moznost vytvorit a editovat uzivatele(pridani roli,...).
    FX2.5: Omezeni pristupu na zaklade konkretnich objektu a roli.
    FX2.6: Moznost dynamickeho nastaveni pristupu ke kazde akci.

# Nefunkcni pozadavky
* N1: Dostupnost aplikace ze vsech modernich prohlizecu.
* N2: Spolehlivost aplikace.
* N3: Podpora pro vice jazykovych verzi.
* N4: PHP minimalne verze 7.0
* N5: Pouziti doctrine ORM pro pristup k databazi.
