Mou semestrální prací bylo vytvoření aplikace pro správu kontaktů a adresních knih včetně
ukládání všech změn v systému. Zpočátku vypadalo téma jednodušše, ale postupem času, kterým
jsem strávil implementací se nakonec ukázalo, že některé věci zaberou většího úsilí.

Zpočátku jsem strávil nějaký čas s hledáním vhodné databázové vrstvy. Nette databáze je moc
jednoduchá a nedá se s ní dělat složitější úkony. Navíc jsem nenašel nějaký modul pro logování
změn. Jelikož již mám nějaké zkušenosti s doctrine a existuje velice dobrý wrapper kdyby, tak jsem
vybral právě toto řešení.

Hlavní úsilí (až po vytvoření modelu) jsem vložil do vytvoření formuláře pro přidání a
editaci kontaktů. Chtěl jsem ho udělat uživatelsky přívětivý a bez zbytečného klikání navíc.
Pro tento formulář jsem použil kdyby/FormReplicator, který mi pomohl s přidáváním a odebíráním
dynamických prvků (adresy, emailu, telefonu,...). Pro výplň adresy jsem použil google
službu mapAutocomplete a select2 pro snazší editaci select políček. Při vykreslování ajaxových
odpovědí jsem narazil na problém s aktivací javascriptových skriptů. Naštěstí do inicializace
skriptu nette se dá vložit skript, který se pak při každém vykreslení zavolá.

Další větší prací bylo logování všech změn. Bohužel jsem pro nette a doctrine nenašel žádný
modul, který by mi s tímto mohl pomoci. Při každé změně jsem nechtěl přidávat log automaticky.
(U kontaktu manipulujeme s více než jedním objektem) Naimplementoval jsem si tedy malé extension,
které injektne DoctrineExtension loggable listener. Tento extension zachycuje změny těsně před
vložením do databáze a vytváří pro každou z nich zvláštní záznam do DB. Dále jsem si pak pohrál s
pohledy, aby byla historie přehlednější.

Pro filtraci a vyhledávání mi bohatě vystačil Ublaboo/datagrid modul.

Pro manipulaci s uživateli jsem se inspiroval v nette dokumentaci a vytvořil jednoduchý model.
Pro prvotní přidání uživatele se hodí kdyby/console, což je wrapper nad symfony/console. S využitím
console jsem naimplementoval příkaz, kterým se dá uživateli nastavit přihlašovací údaje. Dále je
pak v aplikaci zvláštní interface pro manipulaci s uživateli.

V systému potřebujeme nějaká vstupní data. Například štítky pro adresy, telefony, emaily jako
je to například v contacts.google.com službě. Opět je zde doctrine a konkrétně data-fixtures modul.
Napsal jsem další cli příkaz pro spuštění tohoto modulu. A třídy, které se starají o vložení
prvotních dat.

Pro správu scss a js asetů jsem použil gulp a napsal soubor gulpfile.js, tak aby se dal jednoduše
použít. Gulp pro verzování asetů vytváří manifest.json soubor, v kterém se nachází aktuální názvy
assetů. Abych mohl čerpat z manifest souboru napsal jsem latte filtr, který assety převede na název
s hashí. Dále jsem použil yarn pro řešení javaskriptových závislostí.

Zpočátku jsem byl k nette trochu kritický a proto jsem si tento předmět vlastně zapsal, abych zjistil
proč je tak populární v našich vodách. Již nějaký čas pracuji především se symfony, takže jsem tento
projekt vzal jako takové srovnání a snahu vzít si to nejlepší z obou světů. Přes prvotní zklamání,
kde mi přišlo, že spoustu věcí musím otrocky implementovat sám, že neexistuje nějaké řešení na
úrovni frameworku a ani moc společností nechce sdílet svůj kód, jsem byl velice příjemně překvapen
částmi, které v ostatních frameworcích, obsahujících mnohem větší základnu, nejsou. Převážně s
podporou javascriptu, snipetů a komponent. Také "systém modulů" je velice
jednoduchý. Hodně se mi líbí šablonovací systém, který se nesnaží jako většina konkurence rozbíjet
html omáčkou a tagy navíc. n: direktivy jsem využíval v hojném měřítku.

Dlouho jsem ale bojoval s dokumentací na internetu a převážně dokumentací kódu,
která neni v moc dobrém stavu. A upřímně nikdy jsem v ní nenašel, co jsem potřeboval. Převážně se
mi nelíbí spojování několika tříd do jednoho souboru, ve většině případů nedostatečné vysvětlení metody
komentáři a zdá se mi úmyslné ignorování nějakých doporučeních nebo standartů. Dále jsem pak
nepochopil anotaci @inject, která je možná jen pro public proměnné a další menší
možná nepodstatné detaily.

Celkově se mi práce na tomto projektu líbila a příjemně mě překvapila manipulace s nette. Při dobré
znalosti tohoto frameworku a nějaké sadě vlastních modulů(neexistuje tolik up-to-date řešeních) je
to velice dobrý nástroj.