<?php

namespace Test;

use Kdyby\Doctrine\Connection;
use Kdyby\Doctrine\Helpers;
use Nette\DI\Container;
use Tester\TestCase;

/**
 * @author Tomas
 */
class BaseTestCase extends TestCase
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function setUp()
    {
        /** @var Connection $connection */
        $connection = $this->container->getByType(Connection::class);
        $file = __DIR__.'/dump.sql';
        Helpers::loadFromFile($connection, $file);
//        $connection->exec("DROP DATABASE IF EXISTS {$connection->getDatabase()}");
//        $db->exec("CREATE DATABASE {$this->databaseName}");
//        dump($connection);die;

    }
}