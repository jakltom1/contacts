PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE user
(
    id INTEGER,
    username VARCHAR,
    password VARCHAR,
    roles VARCHAR
);
INSERT INTO user VALUES(1,'tomas','$2y$10$9R.wX6B0YgcfCmO.O6UPgOtwbTPzECql9nxds7WKZUFrxc2rswONm','N;');
CREATE TABLE event
(
    id INTEGER,
    contact_id INTEGER,
    custom_label_id INTEGER,
    event_date DATE
);
CREATE TABLE tag
(
    id INTEGER,
    name VARCHAR
);
INSERT INTO tag VALUES(1,'asdf');
INSERT INTO tag VALUES(2,'eew');
CREATE TABLE custom_value
(
    id INTEGER,
    contact_id INTEGER,
    custom_label_id INTEGER,
    custom_field_id INTEGER,
    value VARCHAR
);
CREATE TABLE custom_label
(
    id INTEGER,
    label VARCHAR,
    label_for VARCHAR
);
INSERT INTO custom_label VALUES(5,'Anniversary','event');
INSERT INTO custom_label VALUES(4,'Birthday','event');
INSERT INTO custom_label VALUES(2,'Home','address');
INSERT INTO custom_label VALUES(1,'wefw','address');
INSERT INTO custom_label VALUES(3,'Work','address');
CREATE TABLE custom_field
(
    id INTEGER,
    name VARCHAR,
    regex VARCHAR
);
CREATE TABLE contact_tag
(
    id INTEGER,
    tag_id INTEGER,
    contact_id INTEGER
);
INSERT INTO contact_tag VALUES(1,1,1);
INSERT INTO contact_tag VALUES(2,2,1);
CREATE TABLE contact
(
    id INTEGER,
    address_book_id INTEGER,
    name VARCHAR,
    first_name VARCHAR,
    last_name VARCHAR
);
INSERT INTO contact VALUES(1,4,'Petr Kopriv','Petr','Kopriv');
INSERT INTO contact VALUES(2,4,'zxcz','zxcz',NULL);
CREATE TABLE address_book
(
    id INTEGER,
    name VARCHAR
);
INSERT INTO address_book VALUES(1,'qwe');
INSERT INTO address_book VALUES(4,'aaasasdwwwqq');
INSERT INTO address_book VALUES(5,'qwew2213');
INSERT INTO address_book VALUES(6,'addressBook');
INSERT INTO address_book VALUES(7,'addressBook');
INSERT INTO address_book VALUES(8,'addressBook');
INSERT INTO address_book VALUES(9,'addressBook');
INSERT INTO address_book VALUES(10,'addressBook');
INSERT INTO address_book VALUES(11,'addressBook');
INSERT INTO address_book VALUES(12,'dqwdqw');
CREATE TABLE address
(
    id INTEGER,
    contact_id INTEGER,
    custom_label_id INTEGER,
    street VARCHAR,
    town VARCHAR,
    post_code VARCHAR,
    country VARCHAR,
    name VARCHAR
);
INSERT INTO address VALUES(1,1,1,'','Brdo pri Lukovici','1225','Slovenia','Brdo pri Lukovici, 1225, Slovenia');
INSERT INTO address VALUES(2,2,1,'','','','','Bneid Al Qar, Al Asimah Governate, Kuwait');
CREATE TABLE ext_log_entries
(
    id INTEGER,
    action VARCHAR,
    logged_at DATETIME,
    object_id VARCHAR,
    object_class VARCHAR,
    version INTEGER,
    data VARCHAR,
    username VARCHAR
);
INSERT INTO ext_log_entries VALUES(1,'create','2017-06-17 12:29:37.000000 +00','4','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:6:"zxczxc";}',NULL);
INSERT INTO ext_log_entries VALUES(2,'update','2017-06-17 12:29:48.000000 +00','4','App\Model\Entity\AddressBook',2,'a:1:{s:4:"name";s:3:"aaa";}',NULL);
INSERT INTO ext_log_entries VALUES(3,'update','2017-06-17 12:31:30.000000 +00','4','App\Model\Entity\AddressBook',3,'a:1:{s:4:"name";s:4:"aaas";}','@');
INSERT INTO ext_log_entries VALUES(4,'update','2017-06-17 12:44:05.000000 +00','4','App\Model\Entity\AddressBook',4,'a:1:{s:4:"name";s:7:"aaasasd";}',NULL);
INSERT INTO ext_log_entries VALUES(5,'update','2017-06-17 12:53:51.000000 +00','4','App\Model\Entity\AddressBook',5,'a:1:{s:4:"name";s:9:"aaasasdww";}',NULL);
INSERT INTO ext_log_entries VALUES(6,'update','2017-06-17 12:57:24.000000 +00','4','App\Model\Entity\AddressBook',6,'a:1:{s:4:"name";s:10:"aaasasdwww";}',NULL);
INSERT INTO ext_log_entries VALUES(7,'update','2017-06-17 13:17:31.000000 +00','4','App\Model\Entity\AddressBook',7,'a:1:{s:4:"name";s:12:"aaasasdwwwqq";}',NULL);
INSERT INTO ext_log_entries VALUES(8,'update','2017-06-17 13:17:31.000000 +00','4','App\Model\Entity\AddressBook',7,'a:1:{s:4:"name";s:12:"aaasasdwwwqq";}','tomas');
INSERT INTO ext_log_entries VALUES(9,'create','2017-06-17 14:08:13.000000 +00','1','App\Model\Entity\Address',1,'a:7:{s:6:"street";s:0:"";s:4:"town";s:17:"Brdo pri Lukovici";s:8:"postCode";s:4:"1225";s:7:"country";s:8:"Slovenia";s:4:"name";s:37:"Brdo pri Lukovici, Domžale, Slovenia";s:7:"contact";a:1:{s:2:"id";i:1;}s:11:"customLabel";a:1:{s:2:"id";i:1;}}',NULL);
INSERT INTO ext_log_entries VALUES(10,'create','2017-06-17 14:08:13.000000 +00','1','App\Model\Entity\Tagged',1,'a:2:{s:3:"tag";a:1:{s:2:"id";i:1;}s:7:"contact";a:1:{s:2:"id";i:1;}}',NULL);
INSERT INTO ext_log_entries VALUES(11,'create','2017-06-17 14:08:13.000000 +00','2','App\Model\Entity\Tagged',1,'a:2:{s:3:"tag";a:1:{s:2:"id";i:2;}s:7:"contact";a:1:{s:2:"id";i:1;}}',NULL);
INSERT INTO ext_log_entries VALUES(12,'create','2017-06-17 14:08:13.000000 +00','1','App\Model\Entity\Address',1,'a:7:{s:6:"street";s:0:"";s:4:"town";s:17:"Brdo pri Lukovici";s:8:"postCode";s:4:"1225";s:7:"country";s:8:"Slovenia";s:4:"name";s:37:"Brdo pri Lukovici, Domžale, Slovenia";s:7:"contact";a:1:{s:2:"id";i:1;}s:11:"customLabel";a:1:{s:2:"id";i:1;}}','tomas');
INSERT INTO ext_log_entries VALUES(13,'create','2017-06-17 14:08:13.000000 +00','1','App\Model\Entity\Tagged',1,'a:2:{s:3:"tag";a:1:{s:2:"id";i:1;}s:7:"contact";a:1:{s:2:"id";i:1;}}','tomas');
INSERT INTO ext_log_entries VALUES(14,'create','2017-06-17 14:08:13.000000 +00','2','App\Model\Entity\Tagged',1,'a:2:{s:3:"tag";a:1:{s:2:"id";i:2;}s:7:"contact";a:1:{s:2:"id";i:1;}}','tomas');
INSERT INTO ext_log_entries VALUES(15,'update','2017-06-17 14:14:55.000000 +00','1','App\Model\Entity\Address',2,'a:1:{s:4:"name";s:33:"Brdo pri Lukovici, 1225, Slovenia";}',NULL);
INSERT INTO ext_log_entries VALUES(16,'update','2017-06-17 14:14:55.000000 +00','1','App\Model\Entity\Address',2,'a:1:{s:4:"name";s:33:"Brdo pri Lukovici, 1225, Slovenia";}','tomas');
INSERT INTO ext_log_entries VALUES(17,'update','2017-06-17 14:15:37.000000 +00','1','App\Model\Entity\Contact',1,'a:2:{s:4:"name";s:9:"Petr Kopr";s:8:"lastName";s:4:"Kopr";}',NULL);
INSERT INTO ext_log_entries VALUES(18,'update','2017-06-17 14:15:37.000000 +00','1','App\Model\Entity\Contact',1,'a:2:{s:4:"name";s:9:"Petr Kopr";s:8:"lastName";s:4:"Kopr";}','tomas');
INSERT INTO ext_log_entries VALUES(19,'update','2017-06-17 14:19:16.000000 +00','1','App\Model\Entity\Contact',2,'a:2:{s:4:"name";s:11:"Petr Kopriv";s:8:"lastName";s:6:"Kopriv";}','tomas');
INSERT INTO ext_log_entries VALUES(20,'create','2017-06-17 14:21:55.000000 +00','5','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:4:"qwew";}','tomas');
INSERT INTO ext_log_entries VALUES(21,'update','2017-06-17 14:22:11.000000 +00','5','App\Model\Entity\AddressBook',2,'a:1:{s:4:"name";s:8:"qwew2213";}','tomas');
INSERT INTO ext_log_entries VALUES(22,'create','2017-06-17 14:41:28.000000 +00','2','App\Model\Entity\Contact',1,'a:4:{s:4:"name";s:6:"zxczxc";s:9:"firstName";s:6:"zxczxc";s:8:"lastName";N;s:11:"addressBook";a:1:{s:2:"id";i:4;}}','tomas');
INSERT INTO ext_log_entries VALUES(23,'create','2017-06-17 14:43:27.000000 +00','2','App\Model\Entity\Address',1,'a:7:{s:6:"street";s:0:"";s:4:"town";s:0:"";s:8:"postCode";s:0:"";s:7:"country";s:0:"";s:4:"name";s:15:"wwwwwwwwwwwwwww";s:7:"contact";a:1:{s:2:"id";i:2;}s:11:"customLabel";a:1:{s:2:"id";i:1;}}','tomas');
INSERT INTO ext_log_entries VALUES(24,'update','2017-06-17 14:44:54.000000 +00','2','App\Model\Entity\Address',2,'a:1:{s:4:"name";s:41:"Bneid Al Qar, Al Asimah Governate, Kuwait";}','tomas');
INSERT INTO ext_log_entries VALUES(25,'update','2017-06-17 14:49:01.000000 +00','2','App\Model\Entity\Contact',2,'a:2:{s:4:"name";s:4:"zxcz";s:9:"firstName";s:4:"zxcz";}','tomas');
INSERT INTO ext_log_entries VALUES(26,'create','2017-06-17 15:24:34.000000 +00','2','App\Model\Entity\CustomLabel',1,'a:2:{s:5:"label";s:4:"Home";s:8:"labelFor";s:7:"address";}','CLI');
INSERT INTO ext_log_entries VALUES(27,'create','2017-06-17 15:24:34.000000 +00','3','App\Model\Entity\CustomLabel',1,'a:2:{s:5:"label";s:4:"Work";s:8:"labelFor";s:7:"address";}','CLI');
INSERT INTO ext_log_entries VALUES(28,'create','2017-06-17 15:24:34.000000 +00','4','App\Model\Entity\CustomLabel',1,'a:2:{s:5:"label";s:8:"Birthday";s:8:"labelFor";s:5:"event";}','CLI');
INSERT INTO ext_log_entries VALUES(29,'create','2017-06-17 15:24:34.000000 +00','5','App\Model\Entity\CustomLabel',1,'a:2:{s:5:"label";s:11:"Anniversary";s:8:"labelFor";s:5:"event";}','CLI');
INSERT INTO ext_log_entries VALUES(30,'create','2017-06-17 16:02:12.000000 +00','6','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(31,'create','2017-06-17 16:02:18.000000 +00','7','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(32,'create','2017-06-17 16:02:20.000000 +00','8','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(33,'create','2017-06-17 16:02:21.000000 +00','9','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(34,'create','2017-06-17 17:15:13.000000 +00','10','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(35,'create','2017-06-17 17:15:13.000000 +00','11','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:11:"addressBook";}','CLI');
INSERT INTO ext_log_entries VALUES(36,'create','2017-06-17 20:38:09.000000 +00','12','App\Model\Entity\AddressBook',1,'a:1:{s:4:"name";s:6:"dqwdqw";}','tomas');
INSERT INTO ext_log_entries VALUES(37,'create','2017-06-17 21:40:31.000000 +00','3','App\Model\Entity\Contact',1,'a:4:{s:4:"name";s:6:"wedwed";s:9:"firstName";s:6:"wedwed";s:8:"lastName";N;s:11:"addressBook";a:1:{s:2:"id";i:1;}}','tomas');
COMMIT;
