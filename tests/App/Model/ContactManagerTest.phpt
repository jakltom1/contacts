<?php

namespace Test\App\Model;

use App\Model\AddressBookManager;
use App\Model\ContactManager;
use Nette\DI\Container;
use Nette\Utils\ArrayHash;
use Test\BaseTestCase;
use Tester\Assert;

$container = require __DIR__.'/../../bootstrap.php';

/**
 * @testCase
 */
class ContactManagerTest extends BaseTestCase
{
    /**
     * @var ContactManager $model
     */
    private $manager;

    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->manager = $container->getService('contactManager');
    }

    public function createContact()
    {
        $bookPar = ArrayHash::from([
            'name' => 'addressBook',
        ]);

        $book = $this->container->getService('addressBookManager')->insertAddressBook($bookPar);

        $params = ArrayHash::from([
            'name' => 'Petr Skala',
        ]);

        $book = $this->manager->insertContact($params, $book,
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            [0=>'tag1', 1=>'tag2']);

        return $book;
    }

    public function testInsert()
    {
        $entity   = $this->createContact();
        $dbEntity = $this->manager->getContact($entity->getId());
        Assert::equal($entity->getName(), $dbEntity->getName());
        Assert::equal('Petr', $dbEntity->getFirstName());
        Assert::equal('Skala', $dbEntity->getLastName());
        Assert::equal(2, $entity->getTagged()->count());
        Assert::equal('tag1', $dbEntity->getTags()[0]->getName());
        Assert::equal('tag2', $dbEntity->getTags()[1]->getName());
    }

        public function testEdit()
        {
            $contact = $this->createContact();
            $par = ArrayHash::from([
                'name' => 'Tomas slkdf',
            ]);

            $this->manager->updateContact($contact, $par,
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            ArrayHash::from([]),
            []);
            $fromDb = $this->manager->getContact($contact->getId());
            Assert::equal('Tomas slkdf', $fromDb->getName());
            Assert::equal(0, $fromDb->getTags()->count());
        }

        public function testDelete()
        {
            $entity = $this->createContact();
            $id = $entity->getId();

            $this->manager->deleteEntity($entity);
            $fromDb = $this->manager->getContact($id);
            Assert::equal(null, $fromDb);
        }
}

(new ContactManagerTest($container))->run();
