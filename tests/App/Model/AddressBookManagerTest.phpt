<?php

namespace Test\App\Model;

use App\Model\AddressBookManager;
use Nette\DI\Container;
use Nette\Utils\ArrayHash;
use Test\BaseTestCase;
use Tester\Assert;

$container = require __DIR__.'/../../bootstrap.php';

/**
 * @testCase
 */
class AddressBookManagerTest extends BaseTestCase
{
    /**
     * @var AddressBookManager $model
     */
    private $manager;

    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->manager = $container->getService('addressBookManager');

    }

    public function createAddressBook()
    {
        $bookPar = ArrayHash::from([
            'name' => 'addressBook',
        ]);

        $book  = $this->manager->insertAddressBook($bookPar);

        return $book;
    }

    public function testInsert()
    {
        $book           = $this->createAddressBook();
        $comparableBook = $this->manager->getAddressBook($book->getId());
        Assert::equal($book->getName(), $comparableBook->getName());
    }

    public function testEdit()
    {
        $addressBook = $this->createAddressBook();
        $bookPar = ArrayHash::from([
            'name' => 'new book',
        ]);

        $this->manager->updateAddressBook($addressBook, $bookPar);
        $fromDb = $this->manager->getAddressBook($addressBook->getId());
        Assert::equal($addressBook->getName(), $fromDb->getName());
    }

    public function testDelete()
    {
        $addressBook = $this->createAddressBook();
        $id = $addressBook->getId();

        $this->manager->deleteEntity($addressBook);
        $fromDb = $this->manager->getAddressBook($id);
        Assert::equal(null, $fromDb);
    }
}

(new AddressBookManagerTest($container))->run();
