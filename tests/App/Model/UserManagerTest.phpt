<?php

namespace Test\App\Model;

use App\Model\AddressBookManager;
use App\Model\ContactManager;
use App\Model\UserManager;
use Nette\DI\Container;
use Nette\Utils\ArrayHash;
use Test\BaseTestCase;
use Tester\Assert;

$container = require __DIR__.'/../../bootstrap.php';

/**
 * @testCase
 */
class UserManagerTest extends BaseTestCase
{
    /**
     * @var UserManager $model
     */
    private $manager;

    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->manager = $container->getService('userManager');
    }

    public function createUser()
    {
        $entity = $this->manager->add('superusername', 'psssst', ['ROLE_ADMIN']);

        return $entity;
    }

    public function testInsert()
    {
        $entity   = $this->createUser();
        $dbEntity = $this->manager->getUser($entity->getId());
        Assert::equal('superusername', $dbEntity->getUsername());
        Assert::equal(1, count($entity->getRoles()));
        Assert::equal('ROLE_ADMIN', $entity->getRoles()[0]);
    }

    public function testEdit()
    {
        $entity       = $this->createUser();
        $par          = ArrayHash::from([
            'username' => 'aaaa',
            'password' => 'ssdf',
        ]);
        $par['roles'] = [];
        $password     = $entity->getPassword();

        $this->manager->update($entity, $par);
        $fromDb = $this->manager->getUser($entity->getId());
        Assert::equal('aaaa', $fromDb->getUsername());
        Assert::notEqual($password, $fromDb->getPassword());
        Assert::notEqual('ssdf', $fromDb->getPassword());
        Assert::equal(0, count($fromDb->getRoles()));

    }

    public function testDelete()
    {
        $entity = $this->createUser();
        $id     = $entity->getId();

        $this->manager->deleteEntity($entity);
        $fromDb = $this->manager->getUser($id);
        Assert::equal(null, $fromDb);
    }
}

(new UserManagerTest($container))->run();
