<?php
declare(strict_types=1);

namespace App\Fixtures;

use App\Model\Entity\CustomField;
use App\Model\Entity\CustomLabel;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @author Tomas
 *
 * Load label and custom field data.
 */
class LoadLabelFixtures extends AbstractFixture
{
    /**
     * @param ObjectManager $entityManager
     */
    public function load(ObjectManager $entityManager)
    {
        $labels = [
            [CustomLabel::LABEL_ADDRESS, 'Home'],
            [CustomLabel::LABEL_ADDRESS, 'Work'],
            [CustomLabel::LABEL_EMAIL, 'Home'],
            [CustomLabel::LABEL_EMAIL, 'Work'],
            [CustomLabel::LABEL_PHONE, 'Home'],
            [CustomLabel::LABEL_PHONE, 'Work'],
            [CustomLabel::LABEL_EVENT, 'Birthday'],
            [CustomLabel::LABEL_EVENT, 'Anniversary'],
            [CustomLabel::LABEL_CUSTOM_FIELD, 'Home'],
            [CustomLabel::LABEL_CUSTOM_FIELD, 'Work'],
        ];

        foreach ($labels as $label) {
            $item = new CustomLabel($label[0], $label[1]);
            $entityManager->persist($item);
        }

        $entityManager->persist(new CustomField('Instagram'));
        $entityManager->persist(new CustomField('Facebook'));

        $entityManager->flush();
    }
}