<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Model\AddressBookManager;

class HomepagePresenter extends BasePresenter
{
    /**
     * @inject
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $entityManager;

    /**
     * @var AddressBookManager @inject
     */
    public $manager;

    /**
     * Redirects to the first address book.
     */
    public function renderDefault()
    {
        $addressBooks = $this->manager->getAddressBooks();
        if (empty($addressBooks)) {
            $this->redirect('AddressBook:default');
        } else {
            $this->redirect('AddressBook:show', ['id'=>$addressBooks[0]->getId()]);
        }
    }

}
