<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\UserFormFactory;
use App\Model\Entity\User;
use App\Model\UserManager;
use Doctrine\ORM\EntityNotFoundException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 */
class UserPresenter extends BasePresenter
{
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var User
     */
    private $userEntity;

    /**
     * @var UserFormFactory
     */
    private $userFormFactory;

    /**
     * UserPresenter constructor.
     *
     * @param UserManager     $manager
     * @param UserFormFactory $userFormFactory
     */
    public function __construct(UserManager $manager, UserFormFactory $userFormFactory)
    {
        parent::__construct();
        $this->manager         = $manager;
        $this->userFormFactory = $userFormFactory;
    }

    /**
     * @param $id
     *
     * @throws EntityNotFoundException
     */
    public function actionEdit($id)
    {
        $this->userEntity = $this->manager->getUser((int)$id);
        if ($this->userEntity === null) {
            throw new EntityNotFoundException();
        }
        $form = $this['editForm'];

        $form->setDefaults([
            'username' => $this->userEntity->getUsername(),
            'roles'    => $this->userEntity->getRoles(),
        ]);
    }

    /**
     * @param int $id
     */
    public function actionDelete(int $id)
    {
        $user = $this->manager->getUser($id);

        try {
            $this->manager->delete($user);
        } catch (\Exception $e) {
            $this->flashMessage("Something went wrong!", "danger");
        }
        $this->flashMessage("The user was deleted!", "success");

        $this->redirect('User:default');
    }

    /**
     *
     */
    public function renderDefault()
    {
        $this->template->users = $this->manager->getUsers();
    }

    /**
     * @param int $id
     *
     * @throws EntityNotFoundException
     */
    public function renderShow(int $id) : void
    {
        $userEntity = $this->manager->getUser($id);
        if ($userEntity === null) {
            throw new EntityNotFoundException();
        }
        $this->template->userEntity = $userEntity;
    }

    /**
     *
     */
    public function renderEdit()
    {
        $this->template->userEntity = $this->userEntity;
    }

    /**
     * @return Form
     */
    public function createComponentAddForm()
    {
        $form               = $this->userFormFactory->createAddForm();
        $form->onSuccess[]  = [$this, 'processAddForm'];
        $form->onError[]    = [$this, 'redrawUserForm'];
        $form->onValidate[] = [$this, 'validateUserForm'];

        return $form;
    }

    /**
     * @return Form
     */
    public function createComponentEditForm()
    {
        $form               = $this->userFormFactory->createEditForm();
        $form->onSuccess[]  = [$this, 'processEditForm'];
        $form->onError[]    = [$this, 'redrawUserForm'];
        $form->onValidate[] = [$this, 'validateUserForm'];

        return $form;
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processAddForm(Form $form, ArrayHash $values) : void
    {
        try {
            $userEntity = $this->manager->add($values['username'], $values['password'], $values['roles']);
        } catch (\Exception $e) {
            $this->flashMessage("Something went wrong!", "danger");
        }
        $this->flashMessage("The user was added!", "success");

        $this->redirect('User:show', $userEntity->getId());
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processEditForm(Form $form, ArrayHash $values) : void
    {
        try {
            $this->manager->update($this->userEntity, $values);
        } catch (\Exception $e) {
            $form->addError("Something went wrong!");
        }
        $this->flashMessage("The user was edited!", "success");

        $this->redirect('User:show', $this->userEntity->getId());
    }

    /**
     * @param Form $form
     */
    public function validateUserForm(Form $form)
    {
        $values = $form->getValues();
        if ($this->manager->getUserByUsername($values['username']) !== null &&
            $this->manager->getUserByUsername($values['username'])->getId() !=
            $this->getUser()->getIdentity()->getId()
        ) {
            $form['username']->addError('User with this username already exists.');
        }
    }

    /**
     * Redraw user form snippet.
     */
    public function redrawUserForm()
    {
        if ($this->isAjax()) {
            $this->redrawControl('userForm');
        }
    }

}