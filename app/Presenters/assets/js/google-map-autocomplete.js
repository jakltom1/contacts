// let placeSearch;
// let autocompleteElems = [];
// let componentForm = {
//     street_number: 'short_name',
//     route: 'long_name',
//     locality: 'long_name',
//     administrative_area_level_1: 'short_name',
//     country: 'long_name',
//     postal_code: 'short_name'
// };
//
// function initAutocomplete() {
//     const elements = document.getElementsByClassName('autocomplete');
//     for (let i = 0, max = elements.length; i < max; i++) {
//         autocompleteElems.push(
//             new google.maps.places.Autocomplete(
//             /** @type { !HTMLInputElement} */(elements[i]), {types: ['geocode']})
//         );
//
//         // When the user selects an address from the dropdown, populate the address
//         // fields in the form.
//         let element = elements[i];
//         autocompleteElems[i].addListener('place_changed', () => {
//             // Get the place details from the autocomplete object.
//             let place = autocompleteElems[i].getPlace();
//             let elementClasses = ['street', 'postalCode', 'town', 'country'];
//             let elems = new Map();
//
//             let childrens = element.parentNode.children;
//             for (let i = 0; i < childrens.length; i++) {
//                 let className = childrens[i].className;
//                 if (elementClasses.find(x => x === className) !== undefined) {
//                     childrens[i].value = '';
//                     childrens[i].disabled = false;
//                     elems.set(childrens[i].className, childrens[i]);
//                 }
//             }
//
//             // Get each component of the address from the place details
//             // and fill the corresponding field on the form.
//             for (let i = 0; i < place.address_components.length; i++) {
//                 let addressType = place.address_components[i].types[0];
//
//                 if (componentForm[addressType]) {
//                     let val = place.address_components[i][componentForm[addressType]];
//                     switch (addressType) {
//                         case 'streetNumber':
//                             elems.get('street').value += val;
//                             break;
//                         case 'route':
//                             // street
//                             elems.get('street').value += val;
//                             break;
//                         case 'locality':
//                             // city
//                             elems.get('town').value = val;
//                             break;
//                         case 'country':
//                             // country
//                             elems.get('country').value = val;
//                             break;
//                         case 'postal_code':
//                             // postal code
//                             elems.get('postalCode').value = val;
//                             break;
//                         default:
//                             break;
//                     }
//                 }
//             }
//         });
//     }
//
// }

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
// function geolocate() {
//     if (navigator.geolocation) {
//         navigator.geolocation.getCurrentPosition(function (position) {
//             let geolocation = {
//                 lat: position.coords.latitude,
//                 lng: position.coords.longitude
//             };
//             let circle = new google.maps.Circle({
//                 center: geolocation,
//                 radius: position.coords.accuracy
//             });
//             autocomplete.setBounds(circle.getBounds());
//         });
//     }
// }

// $(function () {
//     $.nette.init();
//     $.nette.ext("initOnAjax", { // Rerender select2 and datepicker after ajax.
//         success: function (payload) {
//             if (!payload.redirect) {
//                 initAutocomplete();
//             }
//         }
//     });
//
//     initAutocomplete();
// });
