<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\AddressBookFormFactory;
use App\Model\AddressBookManager;
use App\Model\ContactManager;
use App\Model\Entity\AddressBook;
use App\Model\TagManager;
use Doctrine\ORM\EntityNotFoundException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Ublaboo\DataGrid\DataGrid;

/**
 * @author Tomas
 */
class AddressBookPresenter extends BasePresenter
{
    /**
     * @var AddressBookManager
     */
    private $manager;

    /**
     * @var ContactManager
     */
    private $contactManager;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var AddressBookFormFactory
     */
    private $addrBookFormFactory;

    /**
     * @var AddressBook
     */
    private $addressBook;

    /**
     * AddressBookPresenter constructor.
     *
     * @param AddressBookManager     $manager
     * @param ContactManager         $contactManager
     * @param TagManager             $tagManager
     * @param AddressBookFormFactory $addressBookFormFactory
     */
    public function __construct(
        AddressBookManager $manager,
        ContactManager $contactManager,
        TagManager $tagManager,
        AddressBookFormFactory $addressBookFormFactory
    )
    {
        $this->manager = $manager;
        $this->contactManager = $contactManager;
        $this->tagManager = $tagManager;
        $this->addrBookFormFactory = $addressBookFormFactory;
    }

    /**
     * @param $id
     *
     * @throws EntityNotFoundException
     */
    public function actionEdit($id)
    {
        $this->addressBook = $this->manager->getAddressBook((int)$id);
        if ($this->addressBook === null) {
            throw new EntityNotFoundException();
        }
        $form = $this['editForm'];

        $form->setDefaults([
            'name' => $this->addressBook->getName(),
        ]);
    }

    /**
     * Render edit template.
     */
    public function renderEdit()
    {
        $this->template->addressBook = $this->addressBook;
    }

    /**
     * Render default template.
     */
    public function renderDefault()
    {
        $this->template->addressBooks = $this->manager->getAddressBooks();
    }

    /**
     * @param int $id
     */
    public function renderShow(int $id)
    {
        $addressBook = $this->manager->getAddressBook($id);
        $contacts    = $this->contactManager->getContactsArray($addressBook);

        $this->template->addressBook = $addressBook;
        $this->template->contacts    = $contacts;
    }

    /**
     * Handle delete signal.
     *
     * @param $id
     */
    public function handleDelete($id)
    {
        $contact = $this->contactManager->getContact($id);
        $this->contactManager->deleteContact($contact);

        $this->flashMessage("Item deleted [$id]", 'success');

        if ($this->isAjax()) {
            $this->redrawControl('flashes');
            $this['actionsGrid']->reload();
        } else {
            $this->redirect('this');
        }
    }

    /**
     * @param int $id
     */
    public function actionDelete(int $id)
    {
        $addressBook = $this->manager->getAddressBook($id);
        if (count($this->contactManager->getContacts($addressBook)) > 0) {
            $this->flashMessage("This address book cannot be deleted, because there are contacts in it.", "danger");
            $this->redirect('AddressBook:default');
        }
        try {
            $this->manager->deleteEntity($addressBook);
        } catch (\Exception $e) {
            $this->flashMessage("Something went wrong!", "danger");
        }
        $this->flashMessage("The address book was deleted!", "success");

        $this->redirect('AddressBook:default');
    }

    /**
     * @return Form
     */
    public function createComponentEditForm()
    {
        $form               = $this->addrBookFormFactory->createEditForm();
        $form->onSuccess[]  = [$this, 'processEditForm'];
        $form->onError[]    = [$this, 'redrawAddressBookForm'];
        $form->onValidate[] = [$this, 'validateAddressBookForm'];

        return $form;
    }

    /**
     * @return Form
     */
    public function createComponentAddForm()
    {
        $form               = $this->addrBookFormFactory->createAddForm();
        $form->onSuccess[]  = [$this, 'processAddForm'];
        $form->onError[]    = [$this, 'redrawAddressBookForm'];
        $form->onValidate[] = [$this, 'validateAddressBookForm'];

        return $form;
    }

    /**
     * Redraw address book form snippet.
     */
    public function redrawAddressBookForm()
    {
        if ($this->isAjax()) {
            $this->redrawControl('addressBookForm');
        }
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processAddForm(Form $form, ArrayHash $values) : void
    {
        try {
            $addrBook = $this->manager->insertAddressBook($values);
        } catch (\Exception $e) {
            $this->flashMessage("Something went wrong!", "danger");
            $this->redirect('AddressBook:add');
        }
        $this->flashMessage("The address book was added!", "success");

        $this->redirect('AddressBook:default');
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processEditForm(Form $form, ArrayHash $values) : void
    {
        try {
            $this->manager->updateAddressBook($this->addressBook, $values);
        } catch (\Exception $e) {
            $form->addError("Something went wrong!");
        }
        $this->flashMessage("The address book was edited!", "success");

        $this->redirect('AddressBook:show', $this->addressBook->getId());
    }

    /**
     * @param Form $form
     */
    public function validateAddressBookForm(Form $form)
    {
        $values = $form->getValues();
        if ($this->manager->getAddressBookByName($values['name']) !== null) {
            $form['name']->addError('Address book with this name already exists.');
        }
    }

    /**
     * @param $name
     */
    public function createComponentSimpleGrid($name)
    {
        $grid = new DataGrid($this, $name);

        $id = (int)$this->getParameter('id');

        $addressBook = $this->manager->getAddressBook($id);

        $query = $this->contactManager->getQueryContacts($id);
        $grid->setDataSource($query);
        $grid->addColumnText('name', 'Name');
        $grid->addFilterText('name', 'Name');
        $grid->addColumnText('tagsString', 'Tags');
        $tags = [];
        $i    = 0;
        foreach ($this->tagManager->findAll() as $tag) {
            $tags[$i++] = $tag->getName();
        }
        $grid->addFilterMultiSelect('tagsString', 'Tags', $tags)
             ->setCondition(function ($doctrine, $value) use ($tags) {
                 foreach ($value as $index) {
                     $doctrine
                         ->leftJoin('c.tagged', 'tagged'.$index)
                         ->leftJoin('tagged'.$index.'.tag', 'tag'.$index)
                         ->andWhere('tag'.$index.'.name = :tagName'.$index)
                         ->setParameter('tagName'.$index, $tags[$index]);
                 }
             });
        if ($this->user->isAllowed('Contact', 'add')) {
            $grid->addToolbarButton('Contact:add', 'New Entry', [
                'addressBookID' => $id,
            ]);
        }

        if ($this->user->isAllowed('Contact', 'add')) {
            $grid->addAction('show', '', 'Contact:show')
                 ->setIcon('eye')
                 ->setTitle('Show')
                 ->setClass('btn btn-xs btn-default');
        }

        if ($this->user->isAllowed('Contact', 'edit')) {
            $grid->addAction('edit', '', 'Contact:edit')
                 ->setIcon('pencil')
                 ->setTitle('Edit')
                 ->setClass('btn btn-xs btn-warning');
        }

        if ($this->user->isAllowed('Contact', 'delete')) {
            $grid->addAction('delete', '', 'Contact:delete')
                 ->setIcon('trash')
                 ->setTitle('Delete')
                 ->setClass('btn btn-xs btn-danger ajax')
                 ->setConfirm('Do you really want to delete row %s?', 'name');
        }
    }
}