<?php

namespace App\Presenters;

use App\Model\TagManager;

/**
 * @author Tomas
 */
class TagPresenter extends BasePresenter
{

    /**
     * @var TagManager
     */
    private $manager;

    /**
     * TagPresenter constructor.
     *
     * @param TagManager $manager
     */
    public function __construct(TagManager $manager) { $this->manager = $manager; }

    /**
     * Render default template.
     */
    public function renderDefault()
    {
        $this->template->tags = $this->manager->findAll();
    }
}