<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms;
use Nette;


/**
 * @author Tomas
 * Class SignPresenter
 */
class SignPresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var Forms\SignInFormFactory
     */
    private $signInFactory;

    /**
     * SignPresenter constructor.
     *
     * @param Forms\SignInFormFactory $signInFactory
     */
    public function __construct(Forms\SignInFormFactory $signInFactory)
    {
        $this->signInFactory = $signInFactory;
    }

    /**
     * Sign-in form factory.
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = $this->signInFactory->create(function () {
            $this->redirect('Homepage:');
        });
        $form->onError[] = [$this, 'redrawSignInForm'];

        return $form;
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->redirect('Sign:in');
    }

    public function redrawSignInForm()
    {
        if ($this->isAjax()) {
            $this->redrawControl('signForm');
        }
    }

}
