<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Components\AddressBookMenuControl;
use App\Components\IAddressBookMenuControlFactory;
use Nette;
use Nette\Application\ForbiddenRequestException;

/**
 * Base presenter for all application presenters.
 */
class BasePresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var IAddressBookMenuControlFactory @inject
     */
    public $menuFactory;

    /**
     * @return AddressBookMenuControl
     */
    protected function createComponentAddressBookMenu()
    {
        return $this->menuFactory->create();
    }

    /**
     * Check authorization rules.
     *
     * @param $element
     *
     * @return void
     * @throws ForbiddenRequestException
     */
    public function checkRequirements($element)
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
        } else {
            foreach ($this->user->getRoles() as $role) {
                if ($this->user->getAuthorizator()->isAllowed($role, $this->name, $this->action)) {
                    return;
                }
            }
            throw new ForbiddenRequestException();
        }
    }
}
