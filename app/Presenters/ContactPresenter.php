<?php
declare(strict_types=1);

namespace App\Presenters;

use App\Forms\ContactFormFactory;
use App\Model\AddressBookManager;
use App\Model\ContactManager;
use App\Model\CustomFieldManager;
use App\Model\Entity\AddressBook;
use App\Model\Entity\Contact;
use App\Model\Entity\Labels;
use App\Model\LabelManager;
use App\Model\TagManager;
use Doctrine\ORM\EntityNotFoundException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 */
class ContactPresenter extends BasePresenter
{
    /**
     * @var ContactManager
     */
    private $contactManager;

    /**
     * @var LabelManager
     */
    private $labelManager;

    /**
     * @var AddressBookManager
     */
    private $addrBookManager;

    /**
     * @var ContactFormFactory
     */
    private $contactFormFactory;

    /**
     * @var CustomFieldManager
     */
    private $customFieldManager;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var AddressBook
     */
    private $addrBook;

    /**
     * @var Contact
     */
    private $contact;

    /**
     * ContactPresenter constructor.
     *
     * @param ContactManager     $contactManager
     * @param LabelManager       $labelManager
     * @param AddressBookManager $addrBookManager
     * @param ContactFormFactory $contactFormFactory
     * @param CustomFieldManager $customFieldManager
     * @param TagManager         $tagManager
     */
    public function __construct(
        ContactManager $contactManager,
        LabelManager $labelManager,
        AddressBookManager $addrBookManager,
        ContactFormFactory $contactFormFactory,
        CustomFieldManager $customFieldManager,
        TagManager $tagManager)
    {
        $this->contactManager     = $contactManager;
        $this->labelManager       = $labelManager;
        $this->addrBookManager    = $addrBookManager;
        $this->contactFormFactory = $contactFormFactory;
        $this->customFieldManager = $customFieldManager;
        $this->tagManager         = $tagManager;
    }

    /**
     * @param int $addressBookID
     *
     * @throws EntityNotFoundException
     */
    public function actionAdd(int $addressBookID) : void
    {
        $this->addrBook = $this->addrBookManager->getAddressBook($addressBookID);
        if ($this->addrBook === null) {
            throw new EntityNotFoundException();
        }
    }

    /**
     * @param int $id
     *
     * @throws EntityNotFoundException
     */
    public function actionEdit(int $id)
    {
        $this->contact = $this->contactManager->getContact($id);
        if ($this->contact === null) {
            throw new EntityNotFoundException();
        }
        $form = $this['editForm'];
        $form->setDefaults($this->contactManager->getFormDefaults($this->contact));
    }

    /**
     * @param int $id
     */
    public function actionDelete(int $id)
    {
        $contact       = $this->contactManager->getContact($id);
        $addressBookID = $contact->getAddressBook()->getId();

        try {
            $this->contactManager->deleteContact($contact);
        } catch (\Exception $e) {
            $this->flashMessage("Something went wrong!", 'danger');
            $this->redirect('AddressBook:show', $addressBookID);
        }

        $this->flashMessage("Contact was deleted.", 'success');
        $this->redirect('AddressBook:show', $addressBookID);
    }

    /**
     * @param int $id
     */
    public function renderShow(int $id) : void
    {
        $this->contact               = $this->contactManager->getContact($id);
        $this->template->contact     = $this->contact;
        $this->template->addressBook = $this->contact->getAddressBook();
    }

    /**
     * Render add template.
     */
    public function renderAdd()
    {
        $labels                       = new Labels($this->labelManager->getAllLabels());
        $this->template->labels       = $labels;
        $this->template->customFields = $this->customFieldManager->findAll();
        $this->template->tags         = $this->tagManager->findAll();
        $this->template->addressBook  = $this->addrBook;
    }

    /**
     * Render edit template.
     */
    public function renderEdit()
    {
        $labels                       = new Labels($this->labelManager->getAllLabels());
        $this->template->labels       = $labels;
        $this->template->customFields = $this->customFieldManager->findAll();
        $this->template->tags         = $this->tagManager->findAll();
        $this->template->addressBook  = $this->contact->getAddressBook();
    }

    /**
     * Redraw contact form snippet.
     */
    public function redrawContactForm()
    {
        if ($this->isAjax()) {
            $this->redrawControl('contactForm');
        }
    }

    /**
     * @return Form
     */
    public function createComponentEditForm()
    {
        $form              = $this->contactFormFactory->createEditForm();
        $form->onSuccess[] = [$this, 'processEditForm'];
        $form->onError[]   = [$this, 'redrawContactForm'];
        $form->onAnchor[]  = [$this, 'redrawContactForm'];

        return $form;
    }

    /**
     * @return Form
     */
    public function createComponentAddForm() : Form
    {
        $form              = $this->contactFormFactory->createAddForm();
        $form->onSuccess[] = [$this, 'processAddForm'];
        $form->onError[]   = [$this, 'redrawContactForm'];
        $form->onAnchor[]  = [$this, 'redrawContactForm'];

        return $form;
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processAddForm(Form $form, ArrayHash $values)
    {
        if ($form['submit']->isSubmittedBy() === true) {
            try {
                $contact = $this->contactManager->insertContact(
                    $values,
                    $this->addrBook,
                    $form['addresses']->values,
                    $form['events']->values,
                    $form['customValue']->values,
                    $form['phone']->values,
                    $form['email']->values,
                    $form['tags']->getRawValue()
                );
            } catch (\Exception $e) {
                $this->flashMessage("Something went wrong!", 'danger');
                $this->redirect('Contact:add');
            }
            $this->flashMessage("The contact was added.");

            $this->redirect('Contact:show', $contact->getId());
        }
    }

    /**
     * @param Form      $form
     * @param ArrayHash $values
     */
    public function processEditForm(Form $form, ArrayHash $values)
    {
        if ($form['submit']->isSubmittedBy() === true) {
            try {
                $this->contactManager->updateContact(
                    $this->contact,
                    $values,
                    $form['addresses']->values,
                    $form['events']->values,
                    $form['customValue']->values,
                    $form['phone']->values,
                    $form['email']->values,
                    $form['tags']->getRawValue()
                );
            } catch (\Exception $e) {
                $this->flashMessage("Something went wrong!", 'danger');
                $this->redirect('Contact:edit', $this->contact->getId());
            }
            $this->flashMessage("The contact was edited.");

            $this->redirect('Contact:show', $this->contact->getId());
        }
    }
}