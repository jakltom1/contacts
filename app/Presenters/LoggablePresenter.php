<?php

namespace App\Presenters;

use Doctrine\ORM\EntityManager;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository;
use Ublaboo\DataGrid\DataGrid;

/**
 * @author Tomas
 */
class LoggablePresenter extends BasePresenter
{
    /**
     * @var LogEntryRepository
     */
    private $repository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * LoggablePresenter constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        parent::__construct();
        $this->repository    = $manager->getRepository('Gedmo\Loggable\Entity\LogEntry');
        $this->entityManager = $manager;
    }

    /**
     * @param int $id
     */
    public function renderShow(int $id)
    {
        $logEntry                 = $this->repository->find($id);
        $this->template->logEntry = $logEntry;
        $this->template->object   = $this->entityManager
            ->getRepository($logEntry->getObjectClass())
            ->find($logEntry->getObjectId());
        //        dump($logEntry->getData());die;
    }

    /**
     * @param $name
     */
    public function createComponentSimpleGrid($name)
    {
        $grid = new DataGrid($this, $name);

        $logEntries = $this->repository->createQueryBuilder('l');

        $grid->setDataSource($logEntries);
        $grid->addColumnText('id', 'Id');
        $grid->addFilterText('id', 'Id');
        $grid->addColumnText('action', 'Action');
        $grid->addFilterText('action', 'Action');


        $grid->addColumnDateTime('loggedAt', 'Logged at')
             ->setFormat('H:i d/m/Y');

        $grid->addFilterDate('loggedAt', 'Logged at');
        $grid->addColumnText('objectId', 'Object id');
        $grid->addFilterText('objectId', 'Object id');
        $grid->addColumnText('objectClass', 'Object class');
        $grid->addFilterText('objectClass', 'Object class');
        $grid->addColumnText('username', 'Username');
        $grid->addFilterText('username', 'Username');


        if ($this->user->isAllowed('Loggable', 'show')) {
            $grid->addAction('show', '', 'Loggable:show')
                 ->setIcon('eye')
                 ->setTitle('Show')
                 ->setClass('btn btn-xs btn-default');
        }
    }

}