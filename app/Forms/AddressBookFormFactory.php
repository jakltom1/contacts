<?php
declare(strict_types=1);

namespace App\Forms;

use App\Model\AddressBookManager;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 */
class AddressBookFormFactory extends Object
{
    /**
     * @var AddressBookManager
     */
    private $manager;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * AddressBookFormFactory constructor.
     *
     * @param AddressBookManager $manager
     * @param FormFactory        $formFactory
     */
    public function __construct(AddressBookManager $manager,
                                FormFactory $formFactory)
    {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
    }

    /**
     * Adds a common fields to the form.
     *
     * @param Form $form
     */
    private function addCommonFields(Form $form) : void
    {
        $form->addText('name', 'Name of the address book')
            ->setRequired('The name field is required.')
        ;
    }

    /**
     * Create a form for adding a new AddressBook.
     *
     * @return Form
     */
    public function createAddForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'submit');

        return $form;
    }

    /**
     * Create a form for edit an existing AddressBook.
     *
     * @return Form
     */
    public function createEditForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'edit');

        return $form;
    }
}