<?php
declare(strict_types=1);

namespace App\Forms;

use App\Model\AddressManager;
use App\Model\ContactManager;
use App\Model\LabelManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

/**
 * @author Tomas
 */
class ContactFormFactory extends Control
{
    /**
     * @var ContactManager
     */
    private $manager;

    /**
     * @var AddressManager
     */
    private $addressManager;

    /**
     * @var LabelManager
     */
    private $labelManager;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * ContactFormFactory constructor.
     *
     * @param ContactManager $manager
     * @param FormFactory    $factory
     * @param AddressManager $addressManager
     * @param LabelManager   $labelManager
     */
    public function __construct(
        ContactManager $manager,
        FormFactory $factory,
        AddressManager $addressManager,
        LabelManager $labelManager
    )
    {
        $this->manager        = $manager;
        $this->formFactory    = $factory;
        $this->addressManager = $addressManager;
        $this->labelManager   = $labelManager;
    }

    /**
     * @param $field
     */
    public function addSubmitButton($field)
    {
        $addEvent       = [$this, 'addElementClicked'];
        $field
            ->addSubmit('add', 'add')
            ->setValidationScope(false)
            ->onClick[] = $addEvent;
    }

    /**
     * @param Form   $form
     * @param string $name
     * @param bool   $withField
     *
     * @return mixed
     */
    public function addCustomValueDynamic(Form $form, string $name, bool $withField = false)
    {
        $removeEvent = [$this, 'removeElementClicked'];

        $customValues = $form->addDynamic($name, function (Container $customValue)
        use ($removeEvent, $withField) {
            $labels = $this->labelManager->getLabelsForCustomField();
            $customValue->addText('value', 'Value');
            $customValue->addText('label', 'Label', $labels)
                        ->addConditionOn($customValue['value'], Form::NOT_EQUAL, "")
                        ->setRequired('The label should not be empty.');

            if ($withField === true) {
                $customValue->addText('field', 'Field')
                            ->addConditionOn($customValue['value'], Form::NOT_EQUAL, "")
                            ->setRequired('The field should not be empty.');
            }
            $customValue
                ->addSubmit('remove', 'Remove')
                ->setValidationScope(false)
                ->onClick[] = $removeEvent;
        });
        $this->addSubmitButton($customValues);

        return $customValues;
    }

    /**
     * Adds a common fields to the form.
     *
     * @param Form $form
     */
    public function addCommonFields(Form $form)
    {
        $form->addText('name', 'Name')
             ->setRequired("This field should not be empty.");
        $form->addText('firstName', 'First name');
        $form->addText('lastName', 'Last name');
        $form->addMultiSelect('tags', 'Tags')
            ->checkAllowedValues = false;

        $removeEvent = [$this, 'removeElementClicked'];

        $emails       = $this->addCustomValueDynamic($form, 'email', false);
        $phones       = $this->addCustomValueDynamic($form, 'phone', false);
        $customValues = $this->addCustomValueDynamic($form, 'customValue', true);

        $addresses = $form->addDynamic('addresses', function (Container $address) use ($removeEvent) {
            $address->addText('address', 'Address');
            $address->addText('label', 'Label')
                    ->addConditionOn($address['address'], Form::NOT_EQUAL, "")
                    ->setRequired('The label should not be empty.');

            $address->addHidden('street', 'Street');
            $address->addHidden('town', 'Town');
            $address->addHidden('country', 'Country');
            $address->addHidden('postalCode', 'Postal code');
            $address
                ->addSubmit('remove', 'Remove')
                ->setValidationScope(false)
                ->onClick[] = $removeEvent;
        });
        $this->addSubmitButton($addresses);

        $events = $form->addDynamic('events', function (Container $event) use ($removeEvent) {
            $labels = $this->labelManager->getLabelsForEvent();
            $event->addText('eventDate', 'Date');
            $event->addText('label', 'Label', $labels)
                  ->addConditionOn($event['eventDate'], Form::NOT_EQUAL, "")
                  ->setRequired('The label should not be empty.');
            $event
                ->addSubmit('remove', 'Remove')
                ->setValidationScope(false)
                ->onClick[] = $removeEvent;
        });
        $this->addSubmitButton($events);
    }

    /**
     * @param SubmitButton $button
     */
    public function removeElementClicked(SubmitButton $button)
    {
        // first parent is container
        // second parent is it's replicator
        $parentElem = $button->parent->parent;
        $parentElem->remove($button->parent, true);
        $this->redrawControl('contactForm');
    }

    public function addElementClicked(SubmitButton $button)
    {
        $parentElem = $button->parent;

        // count how many containers were filled
        if ($parentElem->isAllFilled()) {
            // add one container to replicator
            $button->parent->createOne();
        }

        $this->redrawControl('contactForm');
    }

    /**
     * Create a form for adding a new Contact.
     *
     * @return Form
     */
    public function createAddForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'submit');

        return $form;
    }

    /**
     * Create a form for edit an existing Contact.
     *
     * @return Form
     */
    public function createEditForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'edit');

        return $form;
    }
}