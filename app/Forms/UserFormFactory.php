<?php
declare(strict_types=1);

namespace App\Forms;

use App\Model\UserManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * @author Tomas
 */
class UserFormFactory extends Control
{
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * UserFormFactory constructor.
     *
     * @param UserManager $manager
     * @param FormFactory $formFactory
     */
    public function __construct(
        UserManager $manager,
        FormFactory $formFactory
    ){
        $this->manager     = $manager;
        $this->formFactory = $formFactory;
    }

    /**
     * Adds a common fields to the form.
     *
     * @param Form $form
     */
    private function addCommonFields(Form $form) : void
    {
        $form->addText('username', 'Username')
             ->setRequired('The username field is required.');
        $form->addPassword('password', 'Password');
        $form->addPassword('passwordVerify', 'Repeat password')
             ->setRequired('Fill the password again.')
             ->addRule(Form::EQUAL, 'Passwords don\'t match', $form['password']);
        $form->addMultiSelect('roles', 'Roles', $this->manager->getRoles());
        //        ->addOptionAttributes($this->manager->getRoles());
    }

    /**
     * Create a form for adding a new User.
     *
     * @return Form
     */
    public function createAddForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'submit');

        return $form;
    }

    /**
     * Create a form for edit an existing user.
     *
     * @return Form
     */
    public function createEditForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'edit');

        return $form;
    }
}
