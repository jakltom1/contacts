<?php
declare(strict_types=1);

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\Object;

/**
 * @author Tomas
 */
class AddressFormFactory extends Object
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    public function __construct(FormFactory $factory)
    {
        $this->formFactory = $factory;
    }

    /**
     * Adds common fields to the form.
     *
     * @param Form $form
     */
    private function addCommonFields (Form $form) : void
    {
        $form->addText('country', 'Country');
        $form->addText('town', 'Town');
        $form->addText('street', 'Street');
        $form->addText('postalCode', 'Postal code');
    }

    /**
     * Creates an add form.
     *
     * @return Form
     */
    public function createAddForm() : Form
    {
        $form = $this->formFactory->create();
        $form->addProtection("Security token has expired, please submit the form again.");
        $this->addCommonFields($form);
        $form->addSubmit('submit', 'submit');

        return $form;
    }
}