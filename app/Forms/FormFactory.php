<?php
declare(strict_types=1);

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;

class FormFactory
{
    use Nette\SmartObject;

    /**
     * @return Form
     */
    public function create() : Form
    {
        $form                                            = new Form;
        $renderer                                        = $form->getRenderer();
        $renderer->wrappers['controls']['container']     = null;
        $renderer->wrappers['pair']['container']         = 'div class="form-group"';
        $renderer->wrappers['pair']['.error']            = 'has-error';
        $renderer->wrappers['control']['container']      = 'div class=col-sm-9';
        $renderer->wrappers['label']['container']        = 'div class="col-sm-3 control-label text-right"';
        $renderer->wrappers['control']['description']    = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $renderer->wrappers['error']['container'] = null;
        $renderer->wrappers['error']['item'] = 'div class="alert alert-danger"';
        $form->getElementPrototype()->class('form-horizontal ajax');

        $form->onRender[] = function ($form) {
            foreach ($form->getControls() as $control) {
                $type = $control->getOption('type');
                if ($type === 'button') {
                    $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary btn-sm' : 'btn btn-default btn-sm');
                    $usedPrimary = true;
                } elseif (in_array($type, ['text', 'textarea', 'select'], true)) {
                    $control->getControlPrototype()->addClass('form-control input-sm');
                } elseif (in_array($type, ['checkbox', 'radio'], true)) {
                    $control->getSeparatorPrototype()->setName('div')->addClass($type . ' input-sm');

                }
            }
        };

        return $form;
    }

}
