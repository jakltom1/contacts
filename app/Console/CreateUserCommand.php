<?php
declare(strict_types=1);

namespace App\Console;

use App\Model\DuplicateNameException;
use App\Model\UserManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Creates a user with role admin.
 *
 * @author Tomas
 */
class CreateUserCommand extends Command
{
    /** @var UserManager @inject */
    public $userManager;

    protected function configure()
    {
        $this
            ->setName('app:user:create')
            ->setDescription('Create a user.')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        try {
            $this->userManager->add($username, $password, ['admin']);
        } catch (DuplicateNameException $exc) {
            $output->writeln("<error>The user already exists.</error>");

            return;
        } catch (\Exception $e) {
            $output->writeln("<error>$e The user cannot be create.</error>");

            return;
        }

        $output->writeln("<info>The user was sucessfully created.</info>");
    }
}