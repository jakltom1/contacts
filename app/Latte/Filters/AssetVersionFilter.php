<?php

declare(strict_types=1);

namespace App\Latte\Filters;

use Nette\Object;

/**
 * @author Tomas
 *
 * Converts asset name to the version asset name according to manifest file.
 */
class AssetVersionFilter extends Object
{
    /**
     * @var string
     */
    private $appDir;

    /**
     * AssetVersionFilter constructor.
     *
     * @param string $appDir
     */
    public function __construct($appDir)
    {
        $this->appDir = $appDir;
    }

    /**
     * @param $filename
     *
     * @return string
     * @throws \Exception
     */
    public function __invoke(string $filename) : string
    {
        $manifestPath = $this->appDir.'/Presenters/assets/rev-manifest.json';
        if (!file_exists($manifestPath)) {
            throw new \Exception(sprintf('Cannot find manifest file: "%s"', $manifestPath));
        }
        $paths = json_decode(file_get_contents($manifestPath), true);
        if (!isset($paths[$filename])) {
            throw new \Exception(sprintf('There is no file "%s" in the version manifest!', $filename));
        }

        return $paths[$filename];
    }
}