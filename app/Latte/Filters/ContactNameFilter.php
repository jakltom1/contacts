<?php
declare(strict_types=1);

namespace App\Latte\Filters;

use App\Model\Entity\Contact;
use Nette\Object;

/**
 * Get name of the contact depending on his values.
 *
 * @author Tomas
 */
class ContactNameFilter extends Object
{
    /**
     * @param array $contact
     *
     * @return string
     */
    public function __invoke($contact) : string
    {
        if ($contact instanceof Contact){
            $fullName = $contact->getFirstName()." ".$contact->getLastName();
            $name = $contact->getName();
        }else{
            $fullName = $contact['firstName']." ".$contact['lastName'];
            $name = $contact['name'];
        }

        if ($name === $fullName) {
            return $fullName;
        } else {
            $fullName = $fullName !== '' ? '('.$fullName.')' : '';

            return $name.' <small>'.$fullName.'</small>';
        }
    }
}