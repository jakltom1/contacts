<?php

namespace App\Extension;

use Gedmo\Loggable\LoggableListener;
use Kdyby\Doctrine\DI\IEntityProvider;
use Kdyby\Doctrine\DI\OrmExtension;
use Nette\DI\CompilerExtension;
use ReflectionClass;
use RuntimeException;

/**
 * Properly loads gedmo loggable extension.
 *
 * @author Tomas
 */
class LoggableExtension extends CompilerExtension implements IEntityProvider
{
    /**
     * Register services
     */
    public function loadConfiguration()
    {
        if (!class_exists(OrmExtension::class, true)) {
            throw new RuntimeException('Kdyby\Doctrine\DI\OrmExtension is missing. Please register it first.');
        }
    }

    /**
     * @return array
     */
    public function getEntityMappings()
    {
        $mappings                          = [];
        $loggable                          = new ReflectionClass(LoggableListener::class);
        $mappings['Gedmo\Loggable\Entity'] = dirname($loggable->getFileName()).'/Entity';

        return $mappings;
    }
}