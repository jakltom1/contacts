<?php
declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;

        // authentication routes
        $router[] = new Route('login', 'Sign:in');
        $router[] = new Route('logout', 'Sign:out');
        $router[] = new Route('register', 'Sign:up');

        //$router[] = new Route('<presenter>/<action>', 'Homepage:default');
        $router[] = new Route('', 'Homepage:default');
        $router[] = new Route('address-book/', 'AddressBook:default');
        $router[] = new Route('address-book/add', 'AddressBook:add');
        $router[] = new Route('address-book/edit/<id \d+>', 'AddressBook:edit');
        $router[] = new Route('address-book/show/<id \d+>', 'AddressBook:show');
        $router[] = new Route('address-book/delete/<id \d+>', 'AddressBook:delete');

        $router[] = new Route('contact/add/<addressBookID \d+>', 'Contact:add');
        $router[] = new Route('contact/show/<id \d+>', 'Contact:show');
        $router[] = new Route('contact/edit/<id \d+>', 'Contact:edit');
        $router[] = new Route('contact/delete/<id \d+>', 'Contact:delete');

        $router[] = new Route('user/', 'User:default');
        $router[] = new Route('user/add', 'User:add');
        $router[] = new Route('user/show/<id \d+>', 'User:show');
        $router[] = new Route('user/edit/<id \d+>', 'User:edit');
        $router[] = new Route('user/delete/<id \d+>', 'User:delete');

        $router[] = new Route('log-audit/', 'Loggable:default');
        $router[] = new Route('log-audit/<id \d+>', 'Loggable:show');

        $router[] = new Route('tag/', 'Tag:default');

        return $router;
    }

}
