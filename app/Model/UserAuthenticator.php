<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Nette;
use Nette\Security\Passwords;

/**
 * Users management.
 */
class UserAuthenticator implements Nette\Security\IAuthenticator
{
    use Nette\SmartObject;

    /** @var UserRepository */
    private $userRepository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UserAuthenticator constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->entityManager  = $manager;
    }

    /**
     * Performs an authentication.
     *
     * @param array $credentials
     *
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        $user = $this->entityManager->getRepository('App\Model\Entity\User')->findByUsername($username);
        if ($user === null) {
            throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        } elseif (!Passwords::verify($password, $user->getPassword())) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        } elseif (Passwords::needsRehash($user->getPassword())) {
            $user->setPassword(Passwords::hash($password));
            $this->entityManager->flush();
        }

        return $user;
    }

}

