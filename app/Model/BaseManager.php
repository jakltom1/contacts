<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\BaseEntity;
use Doctrine\ORM\EntityManager;

/**
 * @author Tomas
 */
abstract class BaseManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Flush all changes to the db.
     */
    public function flushChanges() : void
    {
        $this->entityManager->flush();
    }

    /**
     * Insert the given entity to the db.
     *
     * @param mixed $entity
     */
    public function insertEntity(BaseEntity $entity) : void
    {
        $this->entityManager->persist($entity);
        $this->flushChanges();
    }

    /**
     * Insert the given entity to be managed by manager.
     *
     * @param BaseEntity $entity
     */
    public function insertWithoutFlush(BaseEntity $entity) : void
    {
        $this->entityManager->persist($entity);
    }

    /**
     * Delete the given entity from the db.
     *
     * @param mixed $entity
     */
    public function deleteEntity(BaseEntity $entity) : void
    {
        $this->entityManager->remove($entity);
        $this->flushChanges();
    }

    /**
     * Delete the given entity from entity manager.
     *
     * @param mixed $entity
     */
    public function deleteEntityWithoutFlush(BaseEntity $entity) : void
    {
        $this->entityManager->remove($entity);
    }
}