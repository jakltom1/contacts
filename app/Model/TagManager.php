<?php

namespace App\Model;

use App\Model\Entity\Repository\TagRepository;
use App\Model\Entity\Tag;
use Doctrine\ORM\EntityManager;

/**
 * @author Tomas
 */
class TagManager extends BaseManager
{
    /**
     * @var TagRepository
     */
    private $repository;

    /**
     * TagManager constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->entityManager = $manager;
        $this->repository    = $manager->getRepository(Tag::class);
    }

    /**
     * Find a tag by the given name or create a new one.
     *
     * @param string $tagName
     *
     * @return Tag|null|object
     */
    public function getTag(string $tagName)
    {
        $tag = $this->repository->findOneBy(['name' => $tagName]);
        if ($tag === null) {
            $tag = new Tag($tagName);
            $this->insertWithoutFlush($tag);
        }

        return $tag;
    }

    /**
     * Find all tags.
     *
     * @return Tag[]
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }
}