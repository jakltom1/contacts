<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="App\Model\Entity\Repository\AddressRepository")
 */
class Address extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $street;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $town;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $postCode;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $country;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="addresses")
     * @Gedmo\Versioned
     * @var Contact
     */
    private $contact;
    /**
     * @ORM\ManyToOne(targetEntity="CustomLabel")
     * @Gedmo\Versioned
     * @var CustomLabel
     */
    private $customLabel;

    /**
     * Address constructor.
     *
     * @param ArrayHash $values
     */
    public function __construct(ArrayHash $values)
    {
        $this->update($values);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param ArrayHash $values
     */
    public function update(ArrayHash $values)
    {
        $this->name     = $values['address'];
        $this->street   = $values['street'];
        $this->town     = $values['town'];
        $this->postCode = $values['postalCode'];
        $this->country  = $values['country'];
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Get address values in compact way.
     */
    public function getAddressString()
    {
        $array = [];
        if ($this->street !== '') {
            array_push($array, $this->street);
        }
        if ($this->town !== '') {
            array_push($array, $this->town);
        }
        if ($this->postCode !== '') {
            array_push($array, $this->postCode);
        }
        if ($this->country !== '') {
            array_push($array, $this->country);
        }
        $i    = 0;
        $size = count($array);
        if ($size === 1) {
            return $array[0];
        }
        $ret = '';
        foreach ($array as $item) {
            if ($i === 0) {
                $ret = $item;
            } else if ($i === $size - 1) {
                $ret .= ', '.$item;
            } else {
                $ret .= ', '.$item;
            }
            $i++;
        }

        return $ret;
    }

    /**
     * @return string
     */
    public function getStreet() : string
    {
        return $this->getString($this->street);
    }

    /**
     * @return string
     */
    public function getTown() : string
    {
        return $this->getString($this->town);
    }

    /**
     * @return string
     */
    public function getPostCode() : string
    {
        return $this->getString($this->postCode);
    }

    /**
     * @return string
     */
    public function getCountry() : string
    {
        return $this->getString($this->country);
    }

    /**
     * @return Contact
     */
    public function getContact() : Contact
    {
        return $this->contact;
    }

    /**
     * @return CustomLabel|null
     */
    public function getCustomLabel() : ?CustomLabel
    {
        return $this->customLabel;
    }

    /**
     * @param string $street
     *
     * @return Address
     */
    public function setStreet(string $street) : self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @param string $town
     *
     * @return Address
     */
    public function setTown(string $town) : self
    {
        $this->town = $town;

        return $this;
    }

    /**
     * @param string $postCode
     *
     * @return Address
     */
    public function setPostCode(string $postCode) : self
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * @param string $country
     *
     * @return Address
     */
    public function setCountry(string $country) : self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param Contact $contact
     *
     * @return Address
     */
    public function setContact(Contact $contact) : self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @param CustomLabel $customLabel
     *
     * @return Address
     */
    public function setCustomLabel(CustomLabel $customLabel) : self
    {
        $this->customLabel = $customLabel;

        return $this;
    }

    public function getName() : ?string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
}