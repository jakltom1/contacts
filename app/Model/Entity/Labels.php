<?php

namespace App\Model\Entity;

/**
 * The class storing labels.
 *
 * @author Tomas
 */
class Labels
{
    private $labels;

    public function __construct(array $labels)
    {
        $this->labels = $labels;
    }

    public function findBy(string $labelFor)
    {
        return array_filter($this->labels, function(CustomLabel $label) use ($labelFor){
            return $label->getLabelFor() === $labelFor;
        });
    }
}
