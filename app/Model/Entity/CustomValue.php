<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\ArrayHash;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity
 */
class CustomValue extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     *
     * @var string
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="customValues")
     * @Gedmo\Versioned
     * @var Contact
     */
    private $contact;

    /**
     * @ORM\ManyToOne(targetEntity="CustomLabel")
     * @Gedmo\Versioned
     * @var CustomLabel
     */
    private $customLabel;

    /**
     * @ORM\ManyToOne(targetEntity="CustomField")
     * @Gedmo\Versioned
     * @var CustomField
     */
    private $customField;

    public function __construct(ArrayHash $values)
    {
        $this->value = $values['value'];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $value
     *
     * @return CustomValue
     */
    public function setValue(string $value) : self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @param Contact $contact
     *
     * @return CustomValue
     */
    public function setContact(Contact $contact) : self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @param CustomLabel $customLabel
     *
     * @return CustomValue
     */
    public function setCustomLabel(CustomLabel $customLabel) : self
    {
        $this->customLabel = $customLabel;

        return $this;
    }
    /**
     * @return Contact
     */
    public function getContact() : Contact
    {
        return $this->contact;
    }

    /**
     * @return CustomLabel|null
     */
    public function getCustomLabel() : ?CustomLabel
    {
        return $this->customLabel;
    }

    /**
     * @param CustomField $customField
     *
     * @return CustomValue
     */
    public function setCustomField(CustomField $customField) : self
    {
        $this->customField = $customField;

        return $this;
    }

    /**
     * @return CustomField
     */
    public function getCustomField() : CustomField
    {
        return $this->customField;
    }

    /**
     * Update this entity with the given values.
     */
    public function update($values)
    {
        $this->setValue($values['value']);
    }
}