<?php

declare(strict_types=1);

namespace App\Model\Entity\Repository;

use App\Model\Entity\AddressBook;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Tomas
 */
class ContactRepository extends EntityRepository
{
    /**
     * Get contacts that belong to the given address book.
     *
     * @param AddressBook $addressBook
     *
     * @return Query
     */
    public function queryContactsByAddressBook(AddressBook $addressBook) : Query
    {
        return $this->createQueryBuilder('c')
                    ->innerJoin('c.addressBook', 'addrBook')
                    ->where('addrBook = :addrBook')
                    ->setParameter('addrBook', $addressBook)
                    ->getQuery();
    }

    /**
     * @param AddressBook $addressBook
     *
     * @return array
     */
    public function arrayContactsByAddressBook(AddressBook $addressBook) : array
    {
        return $this->queryContactsByAddressBook($addressBook)
                    ->getArrayResult();
    }

    /**
     * @param int $addressBookID
     *
     * @return QueryBuilder
     */
    public function queryContacts(int $addressBookID) : QueryBuilder
    {
        return $this->createQueryBuilder('c')
                    ->innerJoin('c.addressBook', 'addrBook')
                    ->where('addrBook.id = :addrBook')
                    ->setParameter('addrBook', $addressBookID)
                    ->distinct(true);
    }

    /**
     * @param AddressBook $addressBook
     *
     * @return array
     */
    public function findContactsByAddressBook(AddressBook $addressBook) : array
    {
        return $this->queryContactsByAddressBook($addressBook)
                    ->getResult();
    }
}