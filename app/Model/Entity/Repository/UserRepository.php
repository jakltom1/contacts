<?php
declare(strict_types=1);

namespace App\Model\Entity\Repository;

use App\Model\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * @author Tomas
 */
class UserRepository extends EntityRepository
{
    /**
     * Find user by an username.
     *
     * @param string $username
     *
     * @return null|User
     */
    public function findByUsername(string $username) : ?User
    {
        $res =
            $this->createQueryBuilder('u')
                 ->where('u.username = :user')
                 ->setParameter('user', $username)
                 ->getQuery()
                 ->getOneOrNullResult();

        return $res;
    }
}