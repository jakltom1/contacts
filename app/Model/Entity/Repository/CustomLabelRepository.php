<?php
declare(strict_types=1);

namespace App\Model\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Tomas
 */
class CustomLabelRepository extends EntityRepository
{

    /**
     * Get label by the given labelFor parameter.
     *
     * @param string $labelFor
     *
     * @return array
     */
    public function getLabels(string $labelFor) : array
    {
        return $this->createQueryBuilder('label')
            ->select('label.label')
            ->where('label.labelFor = :labelParam')
            ->setParameter('labelParam', $labelFor)
            ->distinct(true)
            ->getQuery()
            ->getScalarResult()
        ;
    }
}