<?php
declare(strict_types=1);

namespace App\Model\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Tomas
 */
class AddressBookRepository extends EntityRepository
{
    /**
     * Get all address books in array format.
     *
     * @return array
     */
    public function arrayAll() : array
    {
        return $this->createQueryBuilder('a')
                    ->getQuery()
                    ->getArrayResult();
    }
}