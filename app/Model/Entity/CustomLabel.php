<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @ORM\Entity(repositoryClass="App\Model\Entity\Repository\CustomLabelRepository")
 * @Gedmo\Loggable
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="label_idx", columns={"label","label_for"})
 * })
 */
class CustomLabel extends BaseEntity
{
    const LABEL_ADDRESS = 'address';
    const LABEL_CUSTOM_FIELD = 'custom_field';
    const LABEL_EVENT = 'event';
    const LABEL_EMAIL = 'email';
    const LABEL_PHONE = 'phone';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $label;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $labelFor;

    public function __construct(string $for, string $label)
    {
        $this->setLabelFor($for);
        $this->setLabel($label);
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return CustomLabel
     */
    public function setLabel(string $label) : self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel() : string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getLabelFor() : string
    {
        return $this->labelFor;
    }

    /**
     * @param string $labelFor
     *
     * @return CustomLabel
     */
    public function setLabelFor(string $labelFor) : self
    {
        $this->labelFor = $labelFor;

        return $this;
    }
}