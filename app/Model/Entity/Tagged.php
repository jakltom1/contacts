<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @ORM\Entity()
 * @Gedmo\Loggable
 * @ORM\Table(name="contact_tag")
 */
class Tagged extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tag")
     * @Gedmo\Versioned
     * @var Tag
     */
    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="tagged")
     * @Gedmo\Versioned
     * @var Contact
     */
    private $contact;

    /**
     * Tagged constructor.
     *
     * @param Contact $contact
     * @param Tag     $tag
     */
    public function __construct(Contact $contact, Tag $tag)
    {
        $this->setTag($tag);
        $this->setContact($contact);
        $contact->addTagged($this);
    }

    public function __toString()
    {
        return "Contact: ".$this->contact->getId().":".$this->contact->getName().
               " tag:".$this->tag->getId().":".$this->tag->getName();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param Tag $tag
     *
     * @return Tagged
     */
    public function setTag(Tag $tag) : self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return Tag
     */
    public function getTag() : Tag
    {
        return $this->tag;
    }

    /**
     * @param Contact $contact
     *
     * @return Tagged
     */
    public function setContact(Contact $contact) : self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact() : Contact
    {
        return $this->contact;
    }

}