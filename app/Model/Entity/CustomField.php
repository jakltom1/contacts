<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="App\Model\Entity\Repository\CustomLabelRepository")
 */
class CustomField extends BaseEntity
{
    const FIELD_PHONE = 'phone';
    const FIELD_EMAIL = 'email';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Versioned
     * @var string
     */
    private $name;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     * @var string
     */
    private $regex;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->getString($this->name);
    }

    /**
     * @param string $name
     *
     * @return CustomField
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegex() : string
    {
        return $this->getString($this->regex);
    }

    /**
     * @param string $pattern
     *
     * @return CustomField
     */
    public function setRegex(string $pattern) : self
    {
        $this->regex = $pattern;

        return $this;
    }
}