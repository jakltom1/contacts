<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\ArrayHash;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity
 */
class Event extends BaseEntity
{
    const EVENT_FORMAT = 'd/m/Y';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="date")
     * @Gedmo\Versioned
     */
    private $eventDate;
    /**
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="events")
     * @Gedmo\Versioned
     * @var Contact
     */
    private $contact;
    /**
     * @ORM\ManyToOne(targetEntity="CustomLabel")
     * @Gedmo\Versioned
     * @var CustomLabel
     */
    private $customLabel;

    public function __construct(ArrayHash $event)
    {
        $this->eventDate = new \DateTime();
        $this->update($event);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->eventDate->format('d/m/Y');
    }

    public function update(ArrayHash $event)
    {
        $this->eventDate = (new \DateTime)->createFromFormat(self::EVENT_FORMAT,$event['eventDate']);
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @return Contact
     */
    public function getContact() : Contact
    {
        return $this->contact;
    }

    /**
     * @return CustomLabel
     */
    public function getCustomLabel() : CustomLabel
    {
        return $this->customLabel;
    }

    /**
     * @param string $eventDate
     *
     * @return Event
     */
    public function setEventDate(string $eventDate) : self
    {
        $this->eventDate->createFromFormat(self::EVENT_FORMAT, $eventDate);

        return $this;
    }

    /**
     * @param Contact $contact
     *
     * @return Event
     */
    public function setContact(Contact $contact) : self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @param CustomLabel $customLabel
     *
     * @return Event
     */
    public function setCustomLabel(CustomLabel $customLabel) : self
    {
        $this->customLabel = $customLabel;

        return $this;
    }
}