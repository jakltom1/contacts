<?php
declare(strict_types=1);

namespace App\Model\Entity;

/**
 * @author Tomas
 */
abstract class BaseEntity
{
    /**
     * @param null|string $parameter
     *
     * @return string
     */
    protected function getString(?string $parameter) : string
    {
        return $parameter === null ? '' : $parameter;
    }
}