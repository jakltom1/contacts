<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Neon\Exception;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="App\Model\Entity\Repository\ContactRepository")
 */
class Contact extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     * @var string
     */
    private $name;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     * @var string
     */
    private $firstName;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     * @var string
     */
    private $lastName;

    /**
     * @ORM\ManyToOne(targetEntity="AddressBook")
     * @Gedmo\Versioned
     * @var AddressBook
     */
    private $addressBook;

    /**
     * @ORM\OneToMany(targetEntity="Tagged", mappedBy="contact")
     * @var Tagged[]
     */
    private $tagged;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="contact")
     * @var Address[]
     */
    private $addresses;
    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="contact")
     * @var Event[]
     */
    private $events;
    /**
     * @ORM\OneToMany(targetEntity="CustomValue", mappedBy="contact")
     * @var CustomValue[]
     */
    private $customValues;

    /**
     * Contact constructor.
     *
     * @param string      $name
     * @param AddressBook $addressBook
     *
     * @throws \Exception
     */
    public function __construct(string $name, AddressBook $addressBook)
    {
        $this->update($name);
        $this->setAddressBook($addressBook);
        $this->tagged       = new ArrayCollection();
        $this->events       = new ArrayCollection();
        $this->addresses    = new ArrayCollection();
        $this->customValues = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @throws \Exception
     */
    public function update(string $name)
    {
        if ($name === '') {
            throw new \Exception('Name should not be empty.');
        }
        $this->name = $name;

        $names = explode(' ', $name);
        switch (count($names)) {
            case 1:
                $this->firstName = $name;
                $this->lastName = "";
                break;
            case 2:
                $this->firstName = $names[0];
                $this->lastName  = $names[1];
                break;
            case 3:
                $this->firstName = $names[0];
                $this->lastName  = $names[1].' '.$names[2];
                break;
            case 4:
                $this->firstName = $names[0].' '.$names[1];
                $this->lastName  = $names[2].' '.$names[3];
                break;
            default:
                $this->name = $name;
        }
    }

    /**
     * @return string
     */
    public function getTagsString() : string
    {
        $tagsString = "";
        $i          = 0;
        /** @var Tag $tag */
        foreach ($this->getTags() as $tag) {
            if ($i++ !== 0) {
                $tagsString .= ', ';
            }
            $tagsString .= $tag->getName();
        }

        return $tagsString;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags() : ArrayCollection
    {
        $tags = [];
        foreach ($this->tagged as $tagged) {
            $tags[] = $tagged->getTag();
        }

        return new ArrayCollection($tags);
    }

    /**
     * @param Tag $tag
     *
     * @return Contact
     */
    public function addTag(Tag $tag) : Contact
    {
        $tagged = new Tagged($this, $tag);
        $tagged->setTag($tag);
        $tagged->setContact($this);

        return $this;
    }

    /**
     * @return AddressBook
     */
    public function getAddressBook() : AddressBook
    {
        return $this->addressBook;
    }

    /**
     * @param AddressBook $addressBook
     *
     * @return Contact
     */
    public function setAddressBook(AddressBook $addressBook) : self
    {
        $this->addressBook = $addressBook;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->getString($this->name);
    }

    /**
     * @param string $name
     *
     * @return Contact
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName() : string
    {
        return $this->getString($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Contact
     */
    public function setFirstName(string $firstName) : self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName() : string
    {
        return $this->getString($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Contact
     */
    public function setLastName(string $lastName) : Contact
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return Collection
     */
    public function getAddresses() : Collection
    {
        return $this->addresses;
    }

    /**
     * @param Address[] $addresses
     *
     * @return Contact
     */
    public function setAddresses(array $addresses) : self
    {
        $this->addresses = $addresses;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEvents() : Collection
    {
        return $this->events;
    }

    /**
     * @param Event[] $events
     *
     * @return Contact
     */
    public function setEvents(array $events) : self
    {
        $this->events = $events;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCustomValues() : Collection
    {
        return $this->customValues;
    }

    /**
     * @param CustomValue[] $customValues
     *
     * @return Contact
     */
    public function setCustomValues(array $customValues) : self
    {
        $this->customValues = $customValues;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTagged() : Collection
    {
        return $this->tagged;
    }

    public function addTagged (Tagged $tagged) : self
    {
        $this->tagged->add($tagged);

        return $this;
    }

}