<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Security\IIdentity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Tomas
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="App\Model\Entity\Repository\UserRepository")
 */
class User extends BaseEntity 	implements IIdentity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string",unique=true)
     * @Gedmo\Versioned
     */
    private $username;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;
    /**
     * @var array
     * @ORM\Column(type="array")
     * @Gedmo\Versioned
     */
    private $roles;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->username;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function setUsername(string $username) : self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return User
     */
    public function setRoles(array $roles = []) : self
    {
        $this->roles = $roles;

        return $this;
    }

}