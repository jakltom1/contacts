<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Repository\UserRepository;
use App\Model\Entity\User;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Nette;
use Nette\Security\Passwords;

/**
 * User manager.
 */
class UserManager extends BaseManager
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    const ROLES = ['admin' => 'admin', 'addressbook' => 'addressbook', 'user' => 'user'];

    /**
     * UserManager constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->userRepository = $manager->getRepository('App\Model\Entity\User');
        $this->entityManager  = $manager;
    }

    /**
     * Adds new user.
     *
     * @param string $username
     * @param string $password
     * @param array  $roles
     *
     * @return User
     * @throws DuplicateNameException
     */
    public function add(string $username, string $password, array $roles = [])
    {
        try {
            $user = new User();
            $user->setUsername($username);
            $user->setPassword(Passwords::hash($password));
            if (count($roles) > 0) {
                $user->setRoles($roles);
            }
            $this->entityManager->persist($user);
            $this->entityManager->flush();

        } catch (UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }

        return $user;
    }

    /**
     * Update the given user.
     *
     * @param User                  $user
     * @param Nette\Utils\ArrayHash $values
     *
     * @return User
     * @throws DuplicateNameException
     */
    public function update(User $user, Nette\Utils\ArrayHash $values)
    {
        try {
            $user->setUsername($values['username']);
            $user->setPassword(Passwords::hash($values['password']));
            $user->setRoles($values['roles']);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }

        return $user;
    }

    /**
     * Get all users.
     *
     * @return array
     */
    public function getUsers() : array
    {
        return $this->userRepository->findAll();
    }

    /**
     * Find user by the given id.
     *
     * @param int $id The id of a user.
     *
     * @return User|null
     */
    public function getUser(int $id) : ?User
    {
        return $this->userRepository->find($id);
    }

    /**
     * Delete a given user from the db.
     *
     * @param User $user
     */
    public function delete(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    public function getRoles() : array
    {
        return $this::ROLES;
    }

    /**
     * @param string $username
     *
     * @return User|null
     */
    public function getUserByUsername(string $username) : ?User
    {
        return $this->userRepository->findOneBy(['username' => $username]);
    }

}

class DuplicateNameException extends \Exception
{
}
