<?php

namespace App\Model;

use App\Model\Entity\CustomField;
use App\Model\Entity\Repository\CustomFieldRepository;
use Doctrine\ORM\EntityManager;

/**
 * @author Tomas
 */
class CustomFieldManager extends BaseManager
{
    /**
     * @var CustomFieldRepository
     */
    private $repository;

    /**
     * ContactManager constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->repository    = $manager->getRepository(CustomField::class);
        $this->entityManager = $manager;
    }

    /**
     * Find all custom fields.
     *
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * Get a field by the given parameters.
     *
     * @param string $field
     *
     * @return CustomField
     */
    public function getField(string $field) : CustomField
    {
        $customField = $this->repository->findOneBy(['name' => $field]);
        if ($customField === null) { // Create a new one if the label is not in DB.
            $customField = new CustomField($field);
            $this->insertWithoutFlush($customField);
        }

        return $customField;
    }
}