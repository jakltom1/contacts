<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\CustomLabel;
use App\Model\Entity\Repository\CustomLabelRepository;
use Doctrine\ORM\EntityManager;

/**
 * @author Tomas
 */
class LabelManager extends BaseManager
{
    /**
     * @var CustomLabelRepository
     */
    private $repository;

    /**
     * ContactManager constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->repository    = $manager->getRepository(CustomLabel::class);
        $this->entityManager = $manager;
    }

    /**
     * Get label by the given parameters.
     *
     * @param string $label
     * @param string $labelFor
     *
     * @return CustomLabel
     */
    public function getLabel(string $label, string $labelFor) : CustomLabel
    {
        $labelObject = $this->repository->findOneBy(['label' => $label, 'labelFor' => $labelFor]);
        if ( $labelObject === null ){ // Create a new one if the label is not in DB.
            $labelObject = new CustomLabel($labelFor, $label);
            $this->insertLabel($labelObject);
        }

        return $labelObject;
    }

    /**
     * Get all labels.
     *
     * @return CustomLabel[]
     */
    public function getAllLabels() : array
    {
        return $this->repository->findAll();
    }

    /**
     * Get labels for the given entity.
     *
     * @return array
     */
    public function getLabelsForAddress() : array
    {
        $ret    = $this->repository->getLabels(CustomLabel::LABEL_ADDRESS);
        $labels = [];

        foreach ($ret as $item) {
            array_push($labels, $item['label']);
        }

        return $labels;
    }

    /**
     * Get labels for the given entity.
     *
     * @return array
     */
    public function getLabelsForEvent() : array
    {
        return $this->repository->getLabels(CustomLabel::LABEL_EVENT);
    }

    /**
     * Get labels for the given entity.
     *
     * @return array
     */
    public function getLabelsForCustomField() : array
    {
        return $this->repository->getLabels(CustomLabel::LABEL_CUSTOM_FIELD);
    }

    /**
     * Insert the given label.
     *
     * @param CustomLabel $label
     */
    public function insertLabel(CustomLabel $label) : void
    {
        $this->insertWithoutFlush($label);
    }

    /**
     * Check if a given label exists in the db.
     *
     * @param string $label
     * @param string $labelFor
     *
     * @return bool
     */
    public function labelExists(string $label, string $labelFor) : bool
    {
        foreach ($this->getAllLabels() as $lbl) {
            if ($lbl->getLabel() === $label && $lbl->getLabelFor() == $labelFor) {
                return true;
            }
        }

        return false;
    }
}