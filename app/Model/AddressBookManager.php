<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\AddressBook;
use App\Model\Entity\Repository\AddressBookRepository;
use Doctrine\ORM\EntityManager;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 */
class AddressBookManager extends BaseManager
{
    /**
     * @var AddressBookRepository
     */
    private $repository;

    /**
     * AddressBookManager constructor.
     *
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->repository    = $manager->getRepository(AddressBook::class);
        $this->entityManager = $manager;
    }

    /**
     * @return AddressBook[]
     */
    public function getAddressBooks() : array
    {
        return $this->repository->findAll();
    }

    /**
     * @return AddressBook[]
     */
    public function getAddressBooksArray() : array
    {
        return $this->repository->arrayAll();
    }

    /**
     * Insert a new address book.
     *
     * @param ArrayHash $values
     *
     * @return AddressBook
     */
    public function insertAddressBook(ArrayHash $values) : AddressBook
    {
        $addrBook = new AddressBook($values['name']);
        $this->insertEntity($addrBook);

        return $addrBook;
    }

    /**
     * Edit an address book identified by the given id.
     *
     * @param AddressBook $book
     * @param ArrayHash   $values
     */
    public function updateAddressBook(AddressBook $book, ArrayHash $values) : void
    {
        $book->setName($values['name']);
        $this->flushChanges();
    }

    /**
     * @param int $id
     *
     * @return AddressBook|null
     */
    public function getAddressBook(int $id)
    {
        /** @var AddressBook|null $addressBook */
        $addressBook = $this->repository->find($id);

        return $addressBook;
    }

    /**
     * @param string $name
     *
     * @return AddressBook|null
     */
    public function getAddressBookByName(string $name)
    {
        /** @var AddressBook|null $addressBook */
        $addressBook = $this->repository->findOneBy(['name' => $name]);

        return $addressBook;
    }
}