<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Address;
use App\Model\Entity\AddressBook;
use App\Model\Entity\BaseEntity;
use App\Model\Entity\Contact;
use App\Model\Entity\CustomField;
use App\Model\Entity\CustomLabel;
use App\Model\Entity\CustomValue;
use App\Model\Entity\Event;
use App\Model\Entity\Repository\ContactRepository;
use App\Model\Entity\Tag;
use App\Model\Entity\Tagged;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Nette\Utils\ArrayHash;

/**
 * @author Tomas
 */
class ContactManager extends BaseManager
{
    /**
     * @var ContactRepository
     */
    private $repository;

    /**
     * @var LabelManager
     */
    private $labelManager;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var CustomFieldManager
     */
    private $fieldManager;

    /**
     * @var array Variable for storing temporal labels, that are not flushed.
     */
    private $labels;

    /**
     * @var array Variable for storing temporal fields, that are not flushed.
     */
    private $fields;

    /**
     * ContactManager constructor.
     *
     * @param EntityManager      $manager
     * @param LabelManager       $labelManager
     * @param CustomFieldManager $fieldManager
     * @param TagManager         $tagManager
     */
    public function __construct(
        EntityManager $manager, LabelManager $labelManager,
        CustomFieldManager $fieldManager,
        TagManager $tagManager)
    {
        $this->repository    = $manager->getRepository(Contact::class);
        $this->entityManager = $manager;
        $this->labelManager  = $labelManager;
        $this->fieldManager  = $fieldManager;
        $this->tagManager    = $tagManager;
    }

    /**
     * Get all contacts for the given address book.
     *
     * @param AddressBook $addressBook
     *
     * @return array
     */
    public function getContactsArray(AddressBook $addressBook) : array
    {
        return $this->repository->arrayContactsByAddressBook($addressBook);
    }

    /**
     * Get all contacts for the given address book.
     *
     * @param AddressBook $addressBook
     *
     * @return array
     */
    public function getContacts(AddressBook $addressBook) : array
    {
        return $this->repository->findBy(['addressBook' => $addressBook]);
    }

    /**
     * Insert a contact item and persist into entity manager.
     *
     * @param Contact          $contact
     * @param BaseEntity|mixed $item
     */
    private function insertContactItem(Contact $contact, BaseEntity $item)
    {
        $this->insertWithoutFlush($item);
        $item->setContact($contact);
    }

    /**
     * Add a tag to a contact.
     *
     * @param Contact $contact
     * @param string  $tagName
     */
    private function addTag(Contact $contact, string $tagName)
    {
        $tag    = $this->tagManager->getTag($tagName);
        $tagged = new Tagged($contact, $tag);
        $this->insertWithoutFlush($tagged);
    }

    /**
     * Get existing field or create a new one.
     *
     * @param string $fieldName
     *
     * @return CustomField|mixed
     */
    private function getField(string $fieldName)
    {
        if (array_key_exists($fieldName, $this->fields)) {
            return $this->fields[$fieldName];
        }

        $customField              = $this->fieldManager->getField($fieldName);
        $this->fields[$fieldName] = $customField;

        return $customField;
    }

    /**
     * Get existing label or create a new one.
     *
     * @param string $label
     * @param string $labelFor
     *
     * @return CustomLabel
     */
    private function getLabel(string $label, string $labelFor)
    {
        if (array_key_exists($label, $this->labels)) {
            if (array_key_exists($labelFor, $this->labels[$label])) {
                return $this->labels[$label][$labelFor];
            }
        }
        $labelEntity                     = $this->labelManager->getLabel($label, $labelFor);
        $this->labels[$label][$labelFor] = $labelEntity;

        return $labelEntity;
    }

    /**
     * Insert a new contact specified by parameters.
     *
     * @param ArrayHash   $values
     * @param AddressBook $addrBook
     * @param ArrayHash   $addresses
     * @param ArrayHash   $events
     * @param ArrayHash   $customValues
     * @param ArrayHash   $phones
     * @param ArrayHash   $emails
     * @param array       $tags
     *
     * @return Contact
     */
    public function insertContact(
        ArrayHash $values,
        AddressBook $addrBook,
        ArrayHash $addresses,
        ArrayHash $events,
        ArrayHash $customValues,
        ArrayHash $phones,
        ArrayHash $emails,
        array $tags) : Contact
    {
        $contact      = new Contact($values['name'], $addrBook);
        $this->labels = [];
        $this->fields = [];

        foreach ($addresses as $address) {
            if ($address['address']) {
                $newAddr = new Address($address);
                $newAddr->setCustomLabel($this->getLabel($address['label'], CustomLabel::LABEL_ADDRESS));
                $this->insertContactItem($contact, $newAddr);
            }
        }

        foreach ($events as $event) {
            if ($event['eventDate']) {
                $newEvent = new Event($event);
                $newEvent->setCustomLabel($this->getLabel($event['label'], CustomLabel::LABEL_EVENT));
                $this->insertContactItem($contact, $newEvent);
            }
        }
        foreach ($customValues as $customValue) {
            $this->addCustomValue($contact, $customValue, $customValue['field'], CustomLabel::LABEL_CUSTOM_FIELD);
        }
        foreach ($phones as $phone) {
            $this->addCustomValue($contact, $phone, CustomField::FIELD_PHONE, CustomLabel::LABEL_PHONE);
        }
        foreach ($emails as $email) {
            $this->addCustomValue($contact, $email, CustomField::FIELD_EMAIL, CustomLabel::LABEL_EMAIL);
        }
        foreach ($tags as $tag) {
            $this->addTag($contact, $tag);
        }
        $this->insertEntity($contact);

        return $contact;
    }

    /**
     * Update the given contact by the given values.
     *
     * @param ArrayHash $values
     * @param ArrayHash $addresses
     * @param ArrayHash $events
     * @param ArrayHash $customValues
     * @param ArrayHash $phones
     * @param ArrayHash $emails
     * @param array     $tags
     *
     * @return Contact
     */
    public function updateContact(
        Contact $contact,
        ArrayHash $values,
        ArrayHash $addresses,
        ArrayHash $events,
        ArrayHash $customValues,
        ArrayHash $phones,
        ArrayHash $emails,
        array $tags) : Contact
    {
        $this->labels = [];
        $this->fields = [];

        /** @var Address $address */
        foreach ($contact->getAddresses() as $address) {
            if ($addresses->offsetExists($address->getId())) {
                $address->update($addresses[$address->getId()]);
                $label = $this->getLabel($addresses[$address->getId()]['label'], CustomLabel::LABEL_ADDRESS);
                $address->setCustomLabel($label);
                $addresses[$address->getId()]['__marked'] = true;
            } else {
                $this->deleteEntityWithoutFlush($address);
            }
        }
        foreach ($addresses as $address) {
            if (!array_key_exists('__marked', $address)) {
                if ($address['address']) {
                    $newAddr = new Address(ArrayHash::from($address));
                    $newAddr->setCustomLabel($this->getLabel($address['label'], CustomLabel::LABEL_ADDRESS));
                    $this->insertContactItem($contact, $newAddr);
                }
            }
        }

        /** @var Event $event */
        foreach ($contact->getEvents() as $event) {
            if ($events->offsetExists($event->getId())) {
                $event->update($events[$event->getId()]);
                $label = $this->getLabel($events[$event->getId()]['label'], CustomLabel::LABEL_EVENT);
                $event->setCustomLabel($label);
                $events[$event->getId()]['__marked'] = true;
            } else {
                $this->deleteEntityWithoutFlush($event);
            }
        }
        foreach ($events as $event) {
            if (!array_key_exists('__marked', $event)) {
                $newEvent = new Event(ArrayHash::from($event));
                $newEvent->setCustomLabel($this->getLabel($event['label'], CustomLabel::LABEL_EVENT));
                $this->insertContactItem($contact, $newEvent);
            }
        }

        $customValuesArr = [];
        foreach ($customValues as $id => $customValue) {
            $customValuesArr[$id]            = $customValue;
            $customValuesArr[$id]["__field"] = $customValue['field'];
            $customValuesArr[$id]["__label"] = CustomLabel::LABEL_CUSTOM_FIELD;
        }
        foreach ($phones as $id => $phone) {
            $customValuesArr[$id]            = $phone;
            $customValuesArr[$id]["__field"] = CustomField::FIELD_PHONE;
            $customValuesArr[$id]["__label"] = CustomLabel::LABEL_PHONE;
        }
        foreach ($emails as $id => $email) {
            $customValuesArr[$id]            = $email;
            $customValuesArr[$id]["__field"] = CustomField::FIELD_EMAIL;
            $customValuesArr[$id]["__label"] = CustomLabel::LABEL_EMAIL;
        }

        /** @var CustomValue $value */
        foreach ($contact->getCustomValues() as $value) {
            if (array_key_exists($value->getId(), $customValuesArr)) {
                $value->update($customValuesArr[$value->getId()]);
                $label = $this->getLabel($customValuesArr[$value->getId()]['label'], $customValuesArr[$value->getId()]['__label']);
                $value->setCustomLabel($label);
                if ($customValuesArr[$value->getId()]['__label'] == CustomLabel::LABEL_CUSTOM_FIELD) {
                    $field = $this->getField($customValuesArr[$value->getId()]['field'], $customValuesArr[$value->getId()]['__field']);
                    $value->setCustomField($field);
                }
                $customValuesArr[$value->getId()]['__marked'] = true;
            } else {
                $this->deleteEntityWithoutFlush($value);
            }
        }

        foreach ($customValuesArr as $value) {
            if (!array_key_exists('__marked', $value)) {
                $this->addCustomValue($contact, ArrayHash::from($value), $value['__field'], $value['__label']);
            }
        }

        foreach ($contact->getTagged() as $tagged) {
            $found = false;
            foreach ($tags as $tag) {
                if ($tag == $tagged->getTag()->getName()) {
                    $found = true;
                    break;
                }
            }
            if ($found === false) {
                $this->deleteEntityWithoutFlush($tagged);
            }
        }

        foreach ($tags as $tag) {
            $found = false;
            foreach ($contact->getTags() as $contactTag) {
                if ($tag == $contactTag->getName()) {
                    $found = true;
                    break;
                }
            }
            if ($found === false) {
                $this->addTag($contact, $tag);
            }
        }

        $contact->update($values['name']);
        $this->flushChanges();

        return $contact;
    }

    /**
     * Add a custom value.
     *
     * @param Contact   $contact
     * @param ArrayHash $formValues
     * @param string    $customField
     * @param string    $label
     */
    private function addCustomValue(Contact $contact, ArrayHash $formValues, string $customField, string $label)
    {
        if ($formValues['value']) {
            $newValue = new CustomValue($formValues);
            $newValue->setCustomField($this->fieldManager->getField($customField));
            $newValue->setCustomLabel($this->getLabel($formValues['label'], $label));
            $this->insertContactItem($contact, $newValue);
        }
    }

    /**
     * Find a contact by id.
     *
     * @param $id
     *
     * @return Contact|null
     */
    public function getContact($id) : ?Contact
    {
        return $this->repository->find($id);
    }

    /**
     * @param int $addressBookID
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryContacts(int $addressBookID)
    {
        return $this->repository->queryContacts($addressBookID);
    }

    /**
     * Find all contacts.
     *
     * @return ArrayCollection
     */
    public function findAll() : ArrayCollection
    {
        return new ArrayCollection($this->repository->findAll());
    }

    /**
     * Delete contact given by id.
     *
     * @param Contact $contact
     */
    public function deleteContact(Contact $contact) : void
    {
        $entities = array_merge(
            $contact->getCustomValues()->toArray(),
            $contact->getAddresses()->toArray(),
            $contact->getEvents()->toArray());
        /** @var CustomValue $customValue */
        foreach ($entities as $entity) {
            $this->deleteEntityWithoutFlush($entity);
        }
        foreach ($contact->getTagged() as $tagged) {
            $this->deleteEntityWithoutFlush($tagged);
        }
        $this->deleteEntity($contact);
    }

    /**
     * Gets default values in format acceptable by form.
     *
     * @param Contact $contact
     *
     * @return array
     */
    public function getFormDefaults(Contact $contact) : array
    {
        $formData['name']      = $contact->getName();
        $formData['firstName'] = $contact->getFirstName();
        $formData['lastName']  = $contact->getLastName();
        //        $formData['addresses'] = [];
        /** @var Address $address */
        foreach ($contact->getAddresses() as $address) {
            $formData['addresses'][$address->getId()] = [
                'address'    => $address->getName(),
                'label'      => $address->getCustomLabel()->getLabel(),
                'street'     => $address->getStreet(),
                'town'       => $address->getTown(),
                'country'    => $address->getCountry(),
                'postalCode' => $address->getPostCode(),
            ];
        }
        /** @var Event $event */
        foreach ($contact->getEvents() as $event) {
            $formData['events'][$event->getId()] = [
                'eventDate' => $event->getEventDate()->format('d/m/Y'),
                'label'     => $event->getCustomLabel()->getLabel(),
            ];
        }

        /** @var CustomValue $customValue */
        foreach ($contact->getCustomValues() as $customValue) {
            $arr = [
                'value' => $customValue->getValue(),
                'label' => $customValue->getCustomLabel()->getLabel(),
            ];
            switch ($customValue->getCustomField()->getName()) {
                case CustomField::FIELD_EMAIL:
                    $formData['email'][$customValue->getId()] = $arr;
                    break;
                case CustomField::FIELD_PHONE:
                    $formData['phone'][$customValue->getId()] = $arr;
                    break;
                default:
                    $arr['field']                                   = $customValue->getCustomField()->getName();
                    $formData['customValue'][$customValue->getId()] = $arr;
            }
        }

        /** @var Tag $tag */
        foreach ($contact->getTags() as $tag) {
            $formData['tags'][$tag->getId()] = $tag->getName();
        }

        return $formData;
    }
}