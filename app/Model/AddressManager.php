<?php
declare(strict_types=1);

namespace App\Model;

use App\Model\Entity\Address;
use App\Model\Entity\CustomLabel;
use App\Model\Entity\Repository\AddressRepository;
use Doctrine\ORM\EntityManager;

/**
 * @author Tomas
 */
class AddressManager extends BaseManager
{
    /**
     * @var AddressRepository
     */
    private $addressRepository;

    public function __construct(EntityManager $manager)
    {
        $this->entityManager     = $manager;
        $this->addressRepository = $manager->getRepository(Address::class);
    }

    public function getLabels()
    {
        return $this->labelRepository->getLabels(CustomLabel::LABEL_ADDRESS);
    }
}