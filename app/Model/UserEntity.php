<?php

namespace App\Model;

use Nette\Security\Identity;
use Nette\Security\User;

/**
 * @author Tomas
 *
 * This class is used as a user wrapper for passing user identity arguments in DI.
 */
class UserEntity
{
    /**
     * @var Identity
     */
    static $user;

    public function setUser(User $user)
    {
        self::$user = $user->getIdentity();
    }

    public function getEntity()
    {
        return $this;
    }

    public function getUsername()
    {
        return self::$user !== null ? self::$user->getUsername() : "CLI";
    }
}