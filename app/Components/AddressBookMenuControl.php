<?php
declare(strict_types=1);

namespace App\Components;

use App\Model\AddressBookManager;
use Nette\Application\UI\Control;

/**
 * @author Tomas
 */
class AddressBookMenuControl extends Control
{
    /**
     * @var AddressBookManager
     */
    private $manager;

    /**
     * @var string
     */
    private $menuTemplate;

    /**
     * AddressBookMenuComponent constructor.
     *
     * @param AddressBookManager $manager
     */
    public function __construct(AddressBookManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->menuTemplate = __DIR__ . '/templates/menu.latte';
    }

    /**
     * Render the menu component.
     *
     * @param int $addressBookId
     */
    public function render(?int $addressBookId = 0)
    {
        $this->template->setFile($this->menuTemplate);
        $this->template->addressBooks = $this->manager->getAddressBooks();
        $this->template->addressBookId = $addressBookId;
        $this->template->render();
    }
}