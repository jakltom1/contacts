<?php
declare(strict_types=1);

namespace App\Components;

/**
 * @author Tomas
 */
interface IAddressBookMenuControlFactory
{
    /**
    * @return AddressBookMenuControl
    */
    public function create();
}